## Celtic coin clustering

This repository contains the following scripts:

    1. data_augmentation.py is for data augmentation
    2. cnn.py is for training a convolutional neural network with pretrained weights
    3. cluster.py is for creating a Clustering and evaluating it.

Examples are given at:

    1. Example_Data_Augmentation
    2. Example_CNN
    3. Example_Cluster

Note that only a few images are given in *Example_Data_Augmentation*. Extracted features are available in *Cluster_Example*.


## System requirements

All scripts are tested on [Pop OS 20.10](https://pop.system76.com/).
They should also work on Ubuntu and  Ubuntu derivatives.
For use on Windows you might have to change a few lines of codes which contain sys commands or path variables, but it could work out of the box.
The following packages are necessary:

    1. Tensorflow    2.3.0
    2. Numpy         1.19.0 or higher
    3. tqdm          4.47.0 or higher
    4. Pandas        1.0.5  or higher  
    5. Matplotlib    3.2.2  or higher
    6. OpenCV        4.3.0  or higher
    7. SciPy         1.5.1  
    8. Scikit-learn  0.23.1 
    9. Seaborn       0.10.1 or higher

Using package manager such as [Miniconda](https://docs.conda.io/en/latest/miniconda.html) are recommended but not  necessary.
On Linux systems use **system76** [Tensorman](https://support.system76.com/articles/use-tensorman/) for  easy Tensorflow GPU support.
The bash command `tensorman +2.3.0 run --gpu bash` lets you run your script with gpu support.
You can install required packages in the docker image via pip3.


### Experimente

*Experimente* contains trained CNNs from the experiments of my bachelor thesis. The CNN  used to extract the feature is included.