import tensorflow as tf
import numpy as np
import os
import csv
import pandas
import matplotlib.pyplot as plt
import matplotlib.cm as cm


class ConvNet():

    def __init__(self, train_path, test_path, model='ResNet50V2',  img_size=(224, 224, 3)):
        """
        train_path: Path to training data. Should be in a tree structure.
        test_path: Path to test data. Should be in a tree structure.
        model: which ResNet model to use. Default ResNet50V2 other options ResNet101V2 and ResNet152V2
        img_size: Input size for the CNN. Default input size of ResNet (224,224,3)
        """
        self.model_name = model
        self.img_size = img_size
        self.num_class = len(os.listdir(train_path))
        self.load_model(name=model)
        self.train_path = train_path
        self.test_path = test_path
        self.classes = os.listdir(self.train_path)
        self.callbacks = []
        self.optimizer = None

    def load_model(self, name):
        """
        Loads a pretrained Model with a dense layer on top. Size of the dense layer equal to the number of classes.
        Other architectures can be added. See https://www.tensorflow.org/api_docs/python/tf/keras/applications/ .
        Don't forget to change the preprocessing function!! 
        """
        if name == 'ResNet50V2':
            # https://www.tensorflow.org/api_docs/python/tf/keras/applications/ResNet50V2
            self.base_model = tf.keras.applications.ResNet50V2(include_top=False, weights='imagenet',
                                                               input_shape=self.img_size, pooling='avg'
                                                               )
        elif name == 'ResNet101V2':
            # https://www.tensorflow.org/api_docs/python/tf/keras/applications/ResNet101V2
            self.base_model = tf.keras.applications.ResNet50V2(include_top=False, weights='imagenet',
                                                               input_shape=self.img_size, pooling='avg'
                                                               )
        elif name == 'ResNet152V2':
            # https://www.tensorflow.org/api_docs/python/tf/keras/applications/ResNet152V2
            self.base_model = tf.keras.applications.ResNet50V2(include_top=False, weights='imagenet',
                                                               input_shape=self.img_size, pooling='avg'
                                                               )
        # https://www.tensorflow.org/api_docs/python/tf/keras/applications/resnet_v2/preprocess_input
        # Only for ResNet! The function must be changed if a different architecture is used.
        self.preprocessing = tf.keras.applications.resnet_v2.preprocess_input

        self.model = tf.keras.Sequential()
        self.model.add(self.base_model)
        self.model.add(tf.keras.layers.Dense(
            self.num_class, activation='softmax'))

    def load_coin_model(self, path):
        """
        Loads a Tensorflow/Keras Model
        Data should be properly preprocessed!! 
        """
        self.model = tf.keras.models.load_model(path)

    def freeze(self, name='conv5'):
        """
        Freeze all layer until a layer named @name appears 
        """
        for layer in self.base_model.layers:
            if layer.name.startswith('conv5'):
                break
            layer.trainable = False

    def set_callbacks(self, callback_list=[], set_default=False):
        """
        Adding callbacks.
        If set_default is true early stopping and checkpoint are added to the list.
        https://www.tensorflow.org/api_docs/python/tf/keras/callbacks list of all callbacks
        """
        self.callbacks += callback_list
        if set_default:
            checkpoint = tf.keras.callbacks.ModelCheckpoint(
                filepath='best_weights.hdf5',
                monitor='val_sparse_categorical_accuracy',
                mode='auto', save_freq='epoch', save_best_only=True)
            early = tf.keras.callbacks.EarlyStopping(
                monitor='val_loss', min_delta=0.001, patience=10, verbose=0, mode='auto',
                baseline=None, restore_best_weights=False)
            self.callbacks.append(early)
            self.callbacks.append(checkpoint)

    def prepare(self, data):
        """
        preprocessing the images
        """
        data = data.map(lambda x, y: (
            self.preprocessing(x), y))
        return data

    def load_dataset(self, batch_size, val_split=0.2, seed=None):
        """
        Function to load and preprocess the datasets.
        batch_size: Batch size for training. Test batch size must be  changed manually in the code!
        val_split: Split size for training/validation split
        https://www.tensorflow.org/api_docs/python/tf/keras/preprocessing/image_dataset_from_directory for more information
        """

        self.train_set = tf.keras.preprocessing.image_dataset_from_directory(
            self.train_path, labels='inferred', label_mode='int', class_names=self.classes,
            color_mode='rgb', batch_size=batch_size, image_size=self.img_size[:-1], shuffle=True, seed=123,
            validation_split=val_split, subset="training", interpolation='bilinear', follow_links=False
        )
        self.val_set = tf.keras.preprocessing.image_dataset_from_directory(
            self.train_path, labels='inferred', label_mode='int', class_names=self.classes,
            color_mode='rgb', batch_size=batch_size,  image_size=self.img_size[:-1], shuffle=True, seed=123,
            validation_split=val_split, subset="validation", interpolation='bilinear', follow_links=False
        )
        self.test_set = tf.keras.preprocessing.image_dataset_from_directory(
            self.test_path,  labels='inferred', label_mode='int', class_names=self.classes,
            color_mode='rgb', batch_size=16, image_size=self.img_size[:-1],
            interpolation='bilinear', follow_links=False
        )

        self.train_set = self.prepare(self.train_set)
        self.val_set = self.prepare(self.val_set)
        self.test_set = self.prepare(self.test_set)

    def load_image(self, path, preprocessing=True):
        """
        Function to load a single Image.
        Intended for testing purposes 
        path: path to image
        preprocessing: Default True. Preprocesses the image with self.preprocess
        """
        image = tf.keras.preprocessing.image.load_img(
            path=path, target_size=self.img_size[:-1])
        img_array = tf.keras.preprocessing.image.img_to_array(image)
        if preprocessing:
            img_array = self.preprocessing(img_array).reshape(-1, 224, 224, 3)
        return img_array

    def train(self, CNN_name="ResNet"):
        """
        Function for training the network.
        5 Epochs with learning rate 0.001
        100 Epochs with learning rate 0.00001
        """
        self.model.compile(optimizer='adam',
                           loss=tf.keras.losses.SparseCategoricalCrossentropy(
                               from_logits=True),
                           metrics=[tf.keras.metrics.SparseCategoricalAccuracy()])
        self.history5 = self.model.fit(self.train_set, epochs=5,
                                       validation_data=self.val_set  # , verbose=2
                                       )
        history_dataframe = pandas.DataFrame(self.history5.history)
        history_csv_file = CNN_name + '_history.csv'
        with open(history_csv_file, mode='w') as his_file:
            history_dataframe.to_csv(his_file)
        test_loss, test_acc = self.model.evaluate(self.test_set  # , verbose=2
                                                  )

        print(CNN_name, 'loss', test_loss, 'acc', test_acc)

        opt = tf.keras.optimizers.Adam(
            learning_rate=0.00001, beta_1=0.9, beta_2=0.999, epsilon=1e-07, amsgrad=False, name='Adam')

        self.model.compile(optimizer=opt,
                           loss=tf.keras.losses.SparseCategoricalCrossentropy(
                               from_logits=True),
                           metrics=[tf.keras.metrics.SparseCategoricalAccuracy()])

        self.history100 = self.model.fit(self.train_set, callbacks=self.callbacks,
                                         epochs=100, validation_data=self.val_set, verbose=2
                                         )
        history_dataframe = pandas.DataFrame(self.history100.history)
        with open(history_csv_file, mode='w') as his_file:
            history_dataframe.to_csv(his_file)

        test_loss, test_acc = self.model.evaluate(self.test_set, verbose=2
                                                  )

        print(CNN_name, 'loss', test_loss, 'acc', test_acc)

        save_name = CNN_name + ".h5"
        self.model.save(save_name)

        print("sucess")


def __main__():
    cnn = ConvNet('train/', 'test/')
    cnn.freeze()
    cnn.set_callbacks([], True)
    cnn.load_dataset(batch_size=128, seed=1)
    # cnn.train()
    path = './test/CATII-H-17544A.jpg'


# if __name__ == '__main__':
#     __main__(()


#   MUSS RAUS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    # def make_gradcam_heatmap(self, img_array, last_conv_layer_name='post_relu', classifier_layer_names=['avg_pool', 'dense']):
    #     """
    #     This function is from https://keras.io/examples/vision/grad_cam/
    #     It was slightly changed.
    #     last_conv_layer_name: to find the name look at self.model.layers[0] !!!!!
    #     classifier_layer_names: should be located at
    #     Default arguments should work for ResNet50V2.
    #     """
    #     # First, we create a model that maps the input image to the activations
    #     # of the last conv layer
    #     last_conv_layer = self.model.layers[0].get_layer(last_conv_layer_name)
    #     last_conv_layer_model = tf.keras.Model(
    #         self.model.layers[0].inputs, last_conv_layer.output)

    #     # Second, we create a model that maps the activations of the last conv
    #     # layer to the final class predictions
    #     classifier_input = tf.keras.Input(
    #         shape=last_conv_layer.output.shape[1:])
    #     x = classifier_input
    #     # Workaround will work for ResNet. 'avg_pool' is located in self.base_model, 'dense' in self.model
    #     workaround = 0
    #     for layer_name in classifier_layer_names:
    #         if workaround == 0:
    #             x = self.model.layers[0].get_layer(layer_name)(x)
    #             workaround += 1
    #         else:
    #             x = self.model.get_layer(layer_name)(x)
    #             classifier_model = tf.keras.Model(classifier_input, x)

    #     # Then, we compute the gradient of the top predicted class for our input image
    #     # with respect to the activations of the last conv layer
    #     with tf.GradientTape() as tape:
    #         # Compute activations of the last conv layer and make the tape watch it
    #         last_conv_layer_output = last_conv_layer_model(img_array)
    #         tape.watch(last_conv_layer_output)
    #         # Compute class predictions
    #         preds = classifier_model(last_conv_layer_output)
    #         top_pred_index = tf.argmax(preds[0])
    #         top_class_channel = preds[:, top_pred_index]

    #     # This is the gradient of the top predicted class with regard to
    #     # the output feature map of the last conv layer
    #     grads = tape.gradient(top_class_channel, last_conv_layer_output)

    #     # This is a vector where each entry is the mean intensity of the gradient
    #     # over a specific feature map channel
    #     pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))

    #     # We multiply each channel in the feature map array
    #     # by "how important this channel is" with regard to the top predicted class
    #     last_conv_layer_output = last_conv_layer_output.numpy()[0]
    #     pooled_grads = pooled_grads.numpy()
    #     for i in range(pooled_grads.shape[-1]):
    #         last_conv_layer_output[:, :, i] *= pooled_grads[i]

    #     # The channel-wise mean of the resulting feature map
    #     # is our heatmap of class activation
    #     heatmap = np.mean(last_conv_layer_output, axis=-1)

    #     # For visualization purpose, we will also normalize the heatmap between 0 & 1
    #     heatmap = np.maximum(heatmap, 0) / np.max(heatmap)
    #     return heatmap

    # def find_label(self, img_name, ground_path):

    #     for folder in os.listdir(ground_path):
    #         folder_path = os.path.join(ground_path, folder)
    #         test_path = os.path.join(folder_path, img_name)
    #         if folder == 'NL':
    #             folder = 'Unlabeled '
    #         if os.path.isfile(test_path):
    #             return folder

    # def display_heatmap(self, img_path, categories_path, model_name,
    #                     last_conv_layer_name='post_relu', classifier_layer_names=['avg_pool', 'dense'],
    #                     save_path='./'):
    #     """
    #     This function is based on https://keras.io/examples/vision/grad_cam/
    #     last_conv_layer_name: to find the name look at self.model.layers[0] !!!!!
    #     classifier_layer_names: should be located at
    #     Default arguments should work for ResNet50V2.
    #     """
    #     # loading image
    #     img_array = self.load_image(img_path)
    #     # generate heatmap
    #     heatmap = self.make_gradcam_heatmap(
    #         img_array, last_conv_layer_name, classifier_layer_names
    #     )
    #     # processing heatmap and saving/displaying image
    #     img = tf.keras.preprocessing.image.load_img(img_path)
    #     img = tf.keras.preprocessing.image.img_to_array(img)
    #     # We rescale heatmap to a range 0-255
    #     heatmap = np.uint8(255 * heatmap)
    #     # We use jet colormap to colorize heatmap
    #     jet = cm.get_cmap("jet")
    #     # We use RGB values of the colormap
    #     jet_colors = jet(np.arange(256))[:, :3]
    #     jet_heatmap = jet_colors[heatmap]
    #     # We create an image with RGB colorized heatmap
    #     jet_heatmap = tf.keras.preprocessing.image.array_to_img(jet_heatmap)
    #     jet_heatmap = jet_heatmap.resize((img.shape[1], img.shape[0]))
    #     jet_heatmap = tf.keras.preprocessing.image.img_to_array(jet_heatmap)
    #     # Superimpose the heatmap on original image
    #     superimposed_img = jet_heatmap * 0.4 + img
    #     # superimpose_array = superimposed_img
    #     superimposed_img = tf.keras.preprocessing.image.array_to_img(
    #         superimposed_img)

    #     ground_truth = self.find_label(
    #         img_path.split('/')[-1], categories_path)

    #     # Save the superimposed image
    #     save_name = 'GradCam_' + model_name + '_GT_' + \
    #         ground_truth + '_' + img_path.split('/')[-1]
    #     save_path = os.path.join(save_path, save_name)
    #     superimposed_img.save(save_path)
