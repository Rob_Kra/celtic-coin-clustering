Num GPUs Available:  1
Best acc:  0.8571428656578064
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 27s - loss: 1.7060 - sparse_categorical_accuracy: 0.8574 - val_loss: 1.9525 - val_sparse_categorical_accuracy: 0.5869
Epoch 2/5
36/36 - 24s - loss: 1.5656 - sparse_categorical_accuracy: 0.9801 - val_loss: 1.8798 - val_sparse_categorical_accuracy: 0.6569
Epoch 3/5
36/36 - 24s - loss: 1.5555 - sparse_categorical_accuracy: 0.9896 - val_loss: 1.6139 - val_sparse_categorical_accuracy: 0.9335
Epoch 4/5
36/36 - 24s - loss: 1.5508 - sparse_categorical_accuracy: 0.9934 - val_loss: 1.6190 - val_sparse_categorical_accuracy: 0.9246
Epoch 5/5
36/36 - 24s - loss: 1.5484 - sparse_categorical_accuracy: 0.9958 - val_loss: 1.5803 - val_sparse_categorical_accuracy: 0.9663
35/35 - 0s - loss: 1.7165 - sparse_categorical_accuracy: 0.8286
ResNet_Test2 loss 1.7165086269378662 acc 0.8285714387893677
Epoch 1/100
36/36 - 25s - loss: 1.5487 - sparse_categorical_accuracy: 0.9947 - val_loss: 1.5574 - val_sparse_categorical_accuracy: 0.9858
Epoch 2/100
36/36 - 24s - loss: 1.5449 - sparse_categorical_accuracy: 0.9982 - val_loss: 1.5524 - val_sparse_categorical_accuracy: 0.9911
Epoch 3/100
36/36 - 24s - loss: 1.5445 - sparse_categorical_accuracy: 0.9987 - val_loss: 1.5502 - val_sparse_categorical_accuracy: 0.9929
Epoch 4/100
36/36 - 25s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5486 - val_sparse_categorical_accuracy: 0.9956
Epoch 5/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5481 - val_sparse_categorical_accuracy: 0.9956
Epoch 6/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5478 - val_sparse_categorical_accuracy: 0.9956
Epoch 7/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5476 - val_sparse_categorical_accuracy: 0.9956
Epoch 8/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5474 - val_sparse_categorical_accuracy: 0.9956
Epoch 9/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5473 - val_sparse_categorical_accuracy: 0.9965
Epoch 10/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5470 - val_sparse_categorical_accuracy: 0.9973
Epoch 11/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5470 - val_sparse_categorical_accuracy: 0.9973
Epoch 12/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5469 - val_sparse_categorical_accuracy: 0.9973
Epoch 13/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5469 - val_sparse_categorical_accuracy: 0.9973
Epoch 14/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5467 - val_sparse_categorical_accuracy: 0.9973
Epoch 15/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5466 - val_sparse_categorical_accuracy: 0.9973
Epoch 16/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5466 - val_sparse_categorical_accuracy: 0.9973
Epoch 17/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5465 - val_sparse_categorical_accuracy: 0.9973
Epoch 18/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5465 - val_sparse_categorical_accuracy: 0.9973
Epoch 19/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5465 - val_sparse_categorical_accuracy: 0.9973
Epoch 20/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5464 - val_sparse_categorical_accuracy: 0.9973
Epoch 21/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5464 - val_sparse_categorical_accuracy: 0.9973
Epoch 22/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5464 - val_sparse_categorical_accuracy: 0.9973
Epoch 23/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5464 - val_sparse_categorical_accuracy: 0.9973
Epoch 24/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5464 - val_sparse_categorical_accuracy: 0.9973
Epoch 25/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5463 - val_sparse_categorical_accuracy: 0.9973
ResNet_Test2 loss 1.6302446126937866 acc 0.9142857193946838
saving 0.9142857193946838
sucess
