Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7068 - sparse_categorical_accuracy: 0.8512 - val_loss: 1.8909 - val_sparse_categorical_accuracy: 0.6498
Epoch 2/5
36/36 - 24s - loss: 1.5659 - sparse_categorical_accuracy: 0.9796 - val_loss: 1.6425 - val_sparse_categorical_accuracy: 0.9007
Epoch 3/5
36/36 - 23s - loss: 1.5590 - sparse_categorical_accuracy: 0.9863 - val_loss: 1.7050 - val_sparse_categorical_accuracy: 0.8395
Epoch 4/5
36/36 - 24s - loss: 1.5520 - sparse_categorical_accuracy: 0.9929 - val_loss: 1.6232 - val_sparse_categorical_accuracy: 0.9211
Epoch 5/5
36/36 - 24s - loss: 1.5481 - sparse_categorical_accuracy: 0.9967 - val_loss: 1.6094 - val_sparse_categorical_accuracy: 0.9335
35/35 - 0s - loss: 1.8134 - sparse_categorical_accuracy: 0.7143
ResNet_Test2 loss 1.8134169578552246 acc 0.7142857313156128
Epoch 1/100
36/36 - 25s - loss: 1.5451 - sparse_categorical_accuracy: 0.9984 - val_loss: 1.5592 - val_sparse_categorical_accuracy: 0.9832
Epoch 2/100
36/36 - 23s - loss: 1.5440 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5502 - val_sparse_categorical_accuracy: 0.9920
Epoch 3/100
36/36 - 25s - loss: 1.5437 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5474 - val_sparse_categorical_accuracy: 0.9956
Epoch 4/100
36/36 - 25s - loss: 1.5437 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5457 - val_sparse_categorical_accuracy: 0.9982
Epoch 5/100
36/36 - 23s - loss: 1.5438 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9982
Epoch 6/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 7/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
Epoch 8/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 9/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
Epoch 10/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 11/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 12/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 13/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 14/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 15/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 16/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 17/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 18/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 19/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 20/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 21/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 22/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 23/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
ResNet_Test2 loss 1.629809021949768 acc 0.9142857193946838
sucess
