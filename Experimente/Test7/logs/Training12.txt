Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5608 files belonging to 11 classes.
Using 4487 files for training.
Found 5608 files belonging to 11 classes.
Using 1121 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7324 - sparse_categorical_accuracy: 0.8213 - val_loss: 1.9362 - val_sparse_categorical_accuracy: 0.6057
Epoch 2/5
36/36 - 27s - loss: 1.5657 - sparse_categorical_accuracy: 0.9806 - val_loss: 1.9570 - val_sparse_categorical_accuracy: 0.5879
Epoch 3/5
36/36 - 25s - loss: 1.5530 - sparse_categorical_accuracy: 0.9922 - val_loss: 1.6715 - val_sparse_categorical_accuracy: 0.8707
Epoch 4/5
36/36 - 27s - loss: 1.5495 - sparse_categorical_accuracy: 0.9949 - val_loss: 1.6213 - val_sparse_categorical_accuracy: 0.9251
Epoch 5/5
36/36 - 25s - loss: 1.5642 - sparse_categorical_accuracy: 0.9822 - val_loss: 1.8655 - val_sparse_categorical_accuracy: 0.6762
35/35 - 0s - loss: 2.1170 - sparse_categorical_accuracy: 0.4000
ResNet_Test10 loss 2.117044687271118 acc 0.4000000059604645
Epoch 1/100
36/36 - 26s - loss: 1.5475 - sparse_categorical_accuracy: 0.9975 - val_loss: 1.6022 - val_sparse_categorical_accuracy: 0.9393
Epoch 2/100
36/36 - 24s - loss: 1.5451 - sparse_categorical_accuracy: 0.9987 - val_loss: 1.5613 - val_sparse_categorical_accuracy: 0.9848
Epoch 3/100
36/36 - 24s - loss: 1.5440 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5516 - val_sparse_categorical_accuracy: 0.9929
Epoch 4/100
36/36 - 25s - loss: 1.5439 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5472 - val_sparse_categorical_accuracy: 0.9964
Epoch 5/100
36/36 - 24s - loss: 1.5436 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5455 - val_sparse_categorical_accuracy: 0.9982
Epoch 6/100
36/36 - 24s - loss: 1.5439 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 7/100
36/36 - 23s - loss: 1.5436 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9982
Epoch 8/100
36/36 - 25s - loss: 1.5438 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5442 - val_sparse_categorical_accuracy: 0.9982
Epoch 9/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5442 - val_sparse_categorical_accuracy: 0.9991
Epoch 10/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5440 - val_sparse_categorical_accuracy: 0.9991
Epoch 11/100
36/36 - 24s - loss: 1.5437 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 0.9991
Epoch 12/100
36/36 - 23s - loss: 1.5436 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 0.9991
Epoch 13/100
36/36 - 25s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 0.9991
Epoch 14/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5443 - val_sparse_categorical_accuracy: 0.9991
Epoch 15/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5442 - val_sparse_categorical_accuracy: 0.9991
Epoch 16/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 0.9991
Epoch 17/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 0.9991
ResNet_Test10 loss 1.658179521560669 acc 0.9142857193946838
sucess
