Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5608 files belonging to 11 classes.
Using 4487 files for training.
Found 5608 files belonging to 11 classes.
Using 1121 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7096 - sparse_categorical_accuracy: 0.8467 - val_loss: 1.8302 - val_sparse_categorical_accuracy: 0.7154
Epoch 2/5
36/36 - 23s - loss: 1.5634 - sparse_categorical_accuracy: 0.9831 - val_loss: 1.7664 - val_sparse_categorical_accuracy: 0.7743
Epoch 3/5
36/36 - 23s - loss: 1.5560 - sparse_categorical_accuracy: 0.9895 - val_loss: 1.5916 - val_sparse_categorical_accuracy: 0.9527
Epoch 4/5
36/36 - 23s - loss: 1.5510 - sparse_categorical_accuracy: 0.9926 - val_loss: 1.5638 - val_sparse_categorical_accuracy: 0.9813
Epoch 5/5
36/36 - 23s - loss: 1.5473 - sparse_categorical_accuracy: 0.9969 - val_loss: 1.5733 - val_sparse_categorical_accuracy: 0.9679
35/35 - 0s - loss: 1.6793 - sparse_categorical_accuracy: 0.8571
ResNet_Test10 loss 1.6792939901351929 acc 0.8571428656578064
Epoch 1/100
36/36 - 24s - loss: 1.5448 - sparse_categorical_accuracy: 0.9982 - val_loss: 1.5527 - val_sparse_categorical_accuracy: 0.9911
Epoch 2/100
36/36 - 24s - loss: 1.5439 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5471 - val_sparse_categorical_accuracy: 0.9964
Epoch 3/100
36/36 - 23s - loss: 1.5436 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5459 - val_sparse_categorical_accuracy: 0.9973
Epoch 4/100
36/36 - 23s - loss: 1.5438 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5454 - val_sparse_categorical_accuracy: 0.9982
Epoch 5/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9982
Epoch 6/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 7/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 8/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
Epoch 9/100
36/36 - 22s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 10/100
36/36 - 23s - loss: 1.5435 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
Epoch 11/100
36/36 - 23s - loss: 1.5438 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
Epoch 12/100
36/36 - 23s - loss: 1.5436 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
Epoch 13/100
36/36 - 23s - loss: 1.5437 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
ResNet_Test10 loss 1.6439756155014038 acc 0.8857142925262451
sucess
