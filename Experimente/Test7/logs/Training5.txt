Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5608 files belonging to 11 classes.
Using 4487 files for training.
Found 5608 files belonging to 11 classes.
Using 1121 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 24s - loss: 1.7153 - sparse_categorical_accuracy: 0.8451 - val_loss: 1.8150 - val_sparse_categorical_accuracy: 0.7261
Epoch 2/5
36/36 - 23s - loss: 1.5789 - sparse_categorical_accuracy: 0.9697 - val_loss: 2.0577 - val_sparse_categorical_accuracy: 0.4826
Epoch 3/5
36/36 - 23s - loss: 1.5524 - sparse_categorical_accuracy: 0.9926 - val_loss: 1.9208 - val_sparse_categorical_accuracy: 0.6173
Epoch 4/5
36/36 - 23s - loss: 1.5459 - sparse_categorical_accuracy: 0.9975 - val_loss: 1.5858 - val_sparse_categorical_accuracy: 0.9572
Epoch 5/5
36/36 - 23s - loss: 1.5490 - sparse_categorical_accuracy: 0.9942 - val_loss: 1.5565 - val_sparse_categorical_accuracy: 0.9875
35/35 - 0s - loss: 1.7447 - sparse_categorical_accuracy: 0.8000
ResNet_Test10 loss 1.7447378635406494 acc 0.800000011920929
Epoch 1/100
36/36 - 24s - loss: 1.5458 - sparse_categorical_accuracy: 0.9980 - val_loss: 1.5498 - val_sparse_categorical_accuracy: 0.9929
Epoch 2/100
36/36 - 24s - loss: 1.5436 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5464 - val_sparse_categorical_accuracy: 0.9955
Epoch 3/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 1.0000
Epoch 4/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5434 - val_sparse_categorical_accuracy: 1.0000
Epoch 5/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5433 - val_sparse_categorical_accuracy: 1.0000
Epoch 6/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 7/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 8/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 9/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 10/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 11/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 12/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 13/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
ResNet_Test10 loss 1.647650122642517 acc 0.9142857193946838
sucess
