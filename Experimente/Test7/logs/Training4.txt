Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5608 files belonging to 11 classes.
Using 4487 files for training.
Found 5608 files belonging to 11 classes.
Using 1121 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 24s - loss: 1.7230 - sparse_categorical_accuracy: 0.8304 - val_loss: 1.8056 - val_sparse_categorical_accuracy: 0.7404
Epoch 2/5
36/36 - 23s - loss: 1.5631 - sparse_categorical_accuracy: 0.9835 - val_loss: 1.7046 - val_sparse_categorical_accuracy: 0.8385
Epoch 3/5
36/36 - 23s - loss: 1.5513 - sparse_categorical_accuracy: 0.9933 - val_loss: 1.5890 - val_sparse_categorical_accuracy: 0.9536
Epoch 4/5
36/36 - 23s - loss: 1.5487 - sparse_categorical_accuracy: 0.9953 - val_loss: 1.5574 - val_sparse_categorical_accuracy: 0.9866
Epoch 5/5
36/36 - 23s - loss: 1.5525 - sparse_categorical_accuracy: 0.9920 - val_loss: 1.5973 - val_sparse_categorical_accuracy: 0.9447
35/35 - 0s - loss: 1.7437 - sparse_categorical_accuracy: 0.8000
ResNet_Test10 loss 1.7437392473220825 acc 0.800000011920929
Epoch 1/100
36/36 - 24s - loss: 1.5511 - sparse_categorical_accuracy: 0.9942 - val_loss: 1.5596 - val_sparse_categorical_accuracy: 0.9857
Epoch 2/100
36/36 - 24s - loss: 1.5458 - sparse_categorical_accuracy: 0.9975 - val_loss: 1.5504 - val_sparse_categorical_accuracy: 0.9938
Epoch 3/100
36/36 - 23s - loss: 1.5449 - sparse_categorical_accuracy: 0.9989 - val_loss: 1.5475 - val_sparse_categorical_accuracy: 0.9955
Epoch 4/100
36/36 - 23s - loss: 1.5442 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5459 - val_sparse_categorical_accuracy: 0.9964
Epoch 5/100
36/36 - 24s - loss: 1.5439 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5454 - val_sparse_categorical_accuracy: 0.9982
Epoch 6/100
36/36 - 23s - loss: 1.5435 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5453 - val_sparse_categorical_accuracy: 0.9973
Epoch 7/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9973
Epoch 8/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9973
Epoch 9/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 10/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
Epoch 11/100
36/36 - 23s - loss: 1.5441 - sparse_categorical_accuracy: 0.9991 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 12/100
36/36 - 23s - loss: 1.5435 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 13/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 14/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
Epoch 15/100
36/36 - 23s - loss: 1.5436 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
Epoch 16/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 17/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 18/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 19/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 20/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 21/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
ResNet_Test10 loss 1.6516540050506592 acc 0.9142857193946838
sucess
