Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 11166 files belonging to 11 classes.
Using 8933 files for training.
Found 11166 files belonging to 11 classes.
Using 2233 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
70/70 - 51s - loss: 1.6523 - sparse_categorical_accuracy: 0.9005 - val_loss: 1.7446 - val_sparse_categorical_accuracy: 0.7980
Epoch 2/5
70/70 - 49s - loss: 1.5571 - sparse_categorical_accuracy: 0.9878 - val_loss: 1.5927 - val_sparse_categorical_accuracy: 0.9503
Epoch 3/5
70/70 - 49s - loss: 1.5491 - sparse_categorical_accuracy: 0.9955 - val_loss: 1.5693 - val_sparse_categorical_accuracy: 0.9754
Epoch 4/5
70/70 - 49s - loss: 1.5507 - sparse_categorical_accuracy: 0.9935 - val_loss: 1.6053 - val_sparse_categorical_accuracy: 0.9373
Epoch 5/5
70/70 - 49s - loss: 1.5455 - sparse_categorical_accuracy: 0.9978 - val_loss: 1.5438 - val_sparse_categorical_accuracy: 0.9991
35/35 - 0s - loss: 1.6195 - sparse_categorical_accuracy: 0.9143
ResNet_Test5 loss 1.6195155382156372 acc 0.9142857193946838
Epoch 1/100
70/70 - 50s - loss: 1.5436 - sparse_categorical_accuracy: 0.9997 - val_loss: 1.5434 - val_sparse_categorical_accuracy: 0.9996
Epoch 2/100
70/70 - 49s - loss: 1.5432 - sparse_categorical_accuracy: 0.9999 - val_loss: 1.5433 - val_sparse_categorical_accuracy: 1.0000
Epoch 3/100
70/70 - 49s - loss: 1.5432 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 4/100
70/70 - 49s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 5/100
70/70 - 49s - loss: 1.5432 - sparse_categorical_accuracy: 0.9999 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 6/100
70/70 - 48s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 7/100
70/70 - 50s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5432 - val_sparse_categorical_accuracy: 1.0000
Epoch 8/100
70/70 - 49s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5431 - val_sparse_categorical_accuracy: 1.0000
Epoch 9/100
70/70 - 50s - loss: 1.5431 - sparse_categorical_accuracy: 0.9999 - val_loss: 1.5431 - val_sparse_categorical_accuracy: 1.0000
Epoch 10/100
70/70 - 49s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5431 - val_sparse_categorical_accuracy: 1.0000
Epoch 11/100
70/70 - 49s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5431 - val_sparse_categorical_accuracy: 1.0000
ResNet_Test5 loss 1.6255993843078613 acc 0.9142857193946838
sucess
