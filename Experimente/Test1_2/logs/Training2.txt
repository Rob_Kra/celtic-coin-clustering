Num GPUs Available:  1
Best acc:  0.8571428656578064
Found 11166 files belonging to 11 classes.
Using 8933 files for training.
Found 11166 files belonging to 11 classes.
Using 2233 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
70/70 - 51s - loss: 1.6725 - sparse_categorical_accuracy: 0.8789 - val_loss: 1.7125 - val_sparse_categorical_accuracy: 0.8307
Epoch 2/5
70/70 - 49s - loss: 1.5549 - sparse_categorical_accuracy: 0.9900 - val_loss: 1.6165 - val_sparse_categorical_accuracy: 0.9275
Epoch 3/5
70/70 - 49s - loss: 1.5516 - sparse_categorical_accuracy: 0.9925 - val_loss: 1.5773 - val_sparse_categorical_accuracy: 0.9673
Epoch 4/5
70/70 - 48s - loss: 1.5484 - sparse_categorical_accuracy: 0.9950 - val_loss: 1.5732 - val_sparse_categorical_accuracy: 0.9691
Epoch 5/5
70/70 - 49s - loss: 1.5457 - sparse_categorical_accuracy: 0.9978 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
35/35 - 0s - loss: 1.6769 - sparse_categorical_accuracy: 0.8571
ResNet_Test5 loss 1.676918864250183 acc 0.8571428656578064
Epoch 1/100
70/70 - 50s - loss: 1.5437 - sparse_categorical_accuracy: 0.9997 - val_loss: 1.5440 - val_sparse_categorical_accuracy: 0.9991
Epoch 2/100
70/70 - 49s - loss: 1.5432 - sparse_categorical_accuracy: 0.9999 - val_loss: 1.5439 - val_sparse_categorical_accuracy: 0.9991
Epoch 3/100
70/70 - 49s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5439 - val_sparse_categorical_accuracy: 0.9991
Epoch 4/100
70/70 - 49s - loss: 1.5431 - sparse_categorical_accuracy: 0.9999 - val_loss: 1.5437 - val_sparse_categorical_accuracy: 0.9991
Epoch 5/100
70/70 - 49s - loss: 1.5432 - sparse_categorical_accuracy: 0.9999 - val_loss: 1.5436 - val_sparse_categorical_accuracy: 0.9996
Epoch 6/100
70/70 - 49s - loss: 1.5432 - sparse_categorical_accuracy: 0.9999 - val_loss: 1.5435 - val_sparse_categorical_accuracy: 0.9996
Epoch 7/100
70/70 - 49s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5435 - val_sparse_categorical_accuracy: 0.9996
Epoch 8/100
70/70 - 49s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5435 - val_sparse_categorical_accuracy: 0.9996
Epoch 9/100
70/70 - 49s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5435 - val_sparse_categorical_accuracy: 0.9996
Epoch 10/100
70/70 - 48s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5435 - val_sparse_categorical_accuracy: 0.9996
Epoch 11/100
70/70 - 48s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5434 - val_sparse_categorical_accuracy: 0.9996
ResNet_Test5 loss 1.6513525247573853 acc 0.8857142925262451
saving 0.8857142925262451
sucess
