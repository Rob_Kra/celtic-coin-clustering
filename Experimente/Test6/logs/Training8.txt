Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7220 - sparse_categorical_accuracy: 0.8357 - val_loss: 1.7538 - val_sparse_categorical_accuracy: 0.7899
Epoch 2/5
36/36 - 24s - loss: 1.5622 - sparse_categorical_accuracy: 0.9836 - val_loss: 1.7209 - val_sparse_categorical_accuracy: 0.8245
Epoch 3/5
36/36 - 24s - loss: 1.5536 - sparse_categorical_accuracy: 0.9911 - val_loss: 1.6719 - val_sparse_categorical_accuracy: 0.8697
Epoch 4/5
36/36 - 24s - loss: 1.5509 - sparse_categorical_accuracy: 0.9940 - val_loss: 1.5599 - val_sparse_categorical_accuracy: 0.9814
Epoch 5/5
36/36 - 23s - loss: 1.5489 - sparse_categorical_accuracy: 0.9947 - val_loss: 1.5668 - val_sparse_categorical_accuracy: 0.9770
35/35 - 0s - loss: 1.7367 - sparse_categorical_accuracy: 0.8000
ResNet_Test9 loss 1.7367196083068848 acc 0.800000011920929
Epoch 1/100
36/36 - 24s - loss: 1.5477 - sparse_categorical_accuracy: 0.9960 - val_loss: 1.5531 - val_sparse_categorical_accuracy: 0.9911
Epoch 2/100
36/36 - 24s - loss: 1.5444 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5480 - val_sparse_categorical_accuracy: 0.9956
Epoch 3/100
36/36 - 24s - loss: 1.5438 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9973
Epoch 4/100
36/36 - 26s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5453 - val_sparse_categorical_accuracy: 0.9982
Epoch 5/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 6/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 7/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 8/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5443 - val_sparse_categorical_accuracy: 0.9991
Epoch 9/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5442 - val_sparse_categorical_accuracy: 0.9991
Epoch 10/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5440 - val_sparse_categorical_accuracy: 1.0000
Epoch 11/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5439 - val_sparse_categorical_accuracy: 1.0000
Epoch 12/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5439 - val_sparse_categorical_accuracy: 1.0000
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5438 - val_sparse_categorical_accuracy: 1.0000
Epoch 14/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5438 - val_sparse_categorical_accuracy: 1.0000
Epoch 15/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5438 - val_sparse_categorical_accuracy: 1.0000
Epoch 16/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5438 - val_sparse_categorical_accuracy: 1.0000
ResNet_Test9 loss 1.6257801055908203 acc 0.9428571462631226
sucess
