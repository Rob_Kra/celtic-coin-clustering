Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 26s - loss: 1.7162 - sparse_categorical_accuracy: 0.8443 - val_loss: 1.7436 - val_sparse_categorical_accuracy: 0.7988
Epoch 2/5
36/36 - 23s - loss: 1.5650 - sparse_categorical_accuracy: 0.9805 - val_loss: 1.7525 - val_sparse_categorical_accuracy: 0.7890
Epoch 3/5
36/36 - 24s - loss: 1.5525 - sparse_categorical_accuracy: 0.9920 - val_loss: 1.6580 - val_sparse_categorical_accuracy: 0.8830
Epoch 4/5
36/36 - 24s - loss: 1.5506 - sparse_categorical_accuracy: 0.9931 - val_loss: 1.5766 - val_sparse_categorical_accuracy: 0.9681
Epoch 5/5
36/36 - 24s - loss: 1.5472 - sparse_categorical_accuracy: 0.9965 - val_loss: 1.5559 - val_sparse_categorical_accuracy: 0.9894
35/35 - 0s - loss: 1.6381 - sparse_categorical_accuracy: 0.9143
ResNet_Test9 loss 1.6381369829177856 acc 0.9142857193946838
Epoch 1/100
36/36 - 24s - loss: 1.5492 - sparse_categorical_accuracy: 0.9942 - val_loss: 1.5476 - val_sparse_categorical_accuracy: 0.9965
Epoch 2/100
36/36 - 24s - loss: 1.5449 - sparse_categorical_accuracy: 0.9982 - val_loss: 1.5467 - val_sparse_categorical_accuracy: 0.9965
Epoch 3/100
36/36 - 24s - loss: 1.5444 - sparse_categorical_accuracy: 0.9989 - val_loss: 1.5459 - val_sparse_categorical_accuracy: 0.9973
Epoch 4/100
36/36 - 24s - loss: 1.5436 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9973
Epoch 5/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9973
Epoch 6/100
36/36 - 25s - loss: 1.5437 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5456 - val_sparse_categorical_accuracy: 0.9973
Epoch 7/100
36/36 - 25s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5453 - val_sparse_categorical_accuracy: 0.9973
Epoch 8/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9973
Epoch 9/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9973
Epoch 10/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9973
Epoch 11/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9973
Epoch 12/100
36/36 - 26s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 13/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 14/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
Epoch 15/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9991
Epoch 16/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
Epoch 17/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
Epoch 18/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
Epoch 19/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
Epoch 20/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 21/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 22/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
ResNet_Test9 loss 1.632051944732666 acc 0.9142857193946838
sucess
