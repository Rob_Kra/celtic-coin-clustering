Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 26s - loss: 1.7192 - sparse_categorical_accuracy: 0.8395 - val_loss: 1.7898 - val_sparse_categorical_accuracy: 0.7598
Epoch 2/5
36/36 - 24s - loss: 1.5592 - sparse_categorical_accuracy: 0.9878 - val_loss: 1.6127 - val_sparse_categorical_accuracy: 0.9326
Epoch 3/5
36/36 - 24s - loss: 1.5521 - sparse_categorical_accuracy: 0.9927 - val_loss: 1.6039 - val_sparse_categorical_accuracy: 0.9424
Epoch 4/5
36/36 - 23s - loss: 1.5508 - sparse_categorical_accuracy: 0.9931 - val_loss: 1.6240 - val_sparse_categorical_accuracy: 0.9211
Epoch 5/5
36/36 - 23s - loss: 1.5543 - sparse_categorical_accuracy: 0.9903 - val_loss: 1.5928 - val_sparse_categorical_accuracy: 0.9521
35/35 - 0s - loss: 1.6838 - sparse_categorical_accuracy: 0.8571
ResNet_Test9 loss 1.6838195323944092 acc 0.8571428656578064
Epoch 1/100
36/36 - 25s - loss: 1.5464 - sparse_categorical_accuracy: 0.9978 - val_loss: 1.5541 - val_sparse_categorical_accuracy: 0.9894
Epoch 2/100
36/36 - 24s - loss: 1.5444 - sparse_categorical_accuracy: 0.9991 - val_loss: 1.5473 - val_sparse_categorical_accuracy: 0.9965
Epoch 3/100
36/36 - 23s - loss: 1.5437 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5467 - val_sparse_categorical_accuracy: 0.9965
Epoch 4/100
36/36 - 24s - loss: 1.5438 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5467 - val_sparse_categorical_accuracy: 0.9965
Epoch 5/100
36/36 - 24s - loss: 1.5436 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5468 - val_sparse_categorical_accuracy: 0.9965
Epoch 6/100
36/36 - 24s - loss: 1.5436 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5468 - val_sparse_categorical_accuracy: 0.9965
Epoch 7/100
36/36 - 25s - loss: 1.5439 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5466 - val_sparse_categorical_accuracy: 0.9965
Epoch 8/100
36/36 - 24s - loss: 1.5435 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5462 - val_sparse_categorical_accuracy: 0.9965
Epoch 9/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5461 - val_sparse_categorical_accuracy: 0.9965
Epoch 10/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5460 - val_sparse_categorical_accuracy: 0.9973
Epoch 11/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5459 - val_sparse_categorical_accuracy: 0.9973
Epoch 12/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5459 - val_sparse_categorical_accuracy: 0.9973
Epoch 13/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5459 - val_sparse_categorical_accuracy: 0.9973
Epoch 14/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9982
Epoch 15/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9982
Epoch 16/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5457 - val_sparse_categorical_accuracy: 0.9982
Epoch 17/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5457 - val_sparse_categorical_accuracy: 0.9973
Epoch 18/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5457 - val_sparse_categorical_accuracy: 0.9973
ResNet_Test9 loss 1.6613346338272095 acc 0.8857142925262451
sucess
