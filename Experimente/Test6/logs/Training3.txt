Num GPUs Available:  1
Best acc:  0.8857142925262451
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7144 - sparse_categorical_accuracy: 0.8468 - val_loss: 1.7252 - val_sparse_categorical_accuracy: 0.8209
Epoch 2/5
36/36 - 23s - loss: 1.5663 - sparse_categorical_accuracy: 0.9790 - val_loss: 1.7813 - val_sparse_categorical_accuracy: 0.7766
Epoch 3/5
36/36 - 23s - loss: 1.5589 - sparse_categorical_accuracy: 0.9867 - val_loss: 1.5790 - val_sparse_categorical_accuracy: 0.9663
Epoch 4/5
36/36 - 23s - loss: 1.5523 - sparse_categorical_accuracy: 0.9916 - val_loss: 1.5722 - val_sparse_categorical_accuracy: 0.9752
Epoch 5/5
36/36 - 23s - loss: 1.5527 - sparse_categorical_accuracy: 0.9911 - val_loss: 1.6735 - val_sparse_categorical_accuracy: 0.8750
35/35 - 0s - loss: 1.7733 - sparse_categorical_accuracy: 0.7714
ResNet_Test9 loss 1.7733148336410522 acc 0.7714285850524902
Epoch 1/100
36/36 - 24s - loss: 1.5478 - sparse_categorical_accuracy: 0.9960 - val_loss: 1.5910 - val_sparse_categorical_accuracy: 0.9521
Epoch 2/100
36/36 - 24s - loss: 1.5454 - sparse_categorical_accuracy: 0.9978 - val_loss: 1.5633 - val_sparse_categorical_accuracy: 0.9814
Epoch 3/100
36/36 - 24s - loss: 1.5443 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5536 - val_sparse_categorical_accuracy: 0.9911
Epoch 4/100
36/36 - 24s - loss: 1.5435 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5512 - val_sparse_categorical_accuracy: 0.9920
Epoch 5/100
36/36 - 24s - loss: 1.5436 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5495 - val_sparse_categorical_accuracy: 0.9938
Epoch 6/100
36/36 - 24s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5487 - val_sparse_categorical_accuracy: 0.9947
Epoch 7/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5481 - val_sparse_categorical_accuracy: 0.9947
Epoch 8/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5478 - val_sparse_categorical_accuracy: 0.9956
Epoch 9/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5473 - val_sparse_categorical_accuracy: 0.9965
Epoch 10/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5470 - val_sparse_categorical_accuracy: 0.9965
Epoch 11/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5470 - val_sparse_categorical_accuracy: 0.9965
Epoch 12/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5464 - val_sparse_categorical_accuracy: 0.9973
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5461 - val_sparse_categorical_accuracy: 0.9973
Epoch 14/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9973
Epoch 15/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5457 - val_sparse_categorical_accuracy: 0.9973
Epoch 16/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5456 - val_sparse_categorical_accuracy: 0.9973
Epoch 17/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5456 - val_sparse_categorical_accuracy: 0.9973
Epoch 18/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5455 - val_sparse_categorical_accuracy: 0.9973
Epoch 19/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5454 - val_sparse_categorical_accuracy: 0.9982
Epoch 20/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5454 - val_sparse_categorical_accuracy: 0.9982
Epoch 21/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5454 - val_sparse_categorical_accuracy: 0.9982
Epoch 22/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5453 - val_sparse_categorical_accuracy: 0.9982
Epoch 23/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5454 - val_sparse_categorical_accuracy: 0.9982
Epoch 24/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
ResNet_Test9 loss 1.6173380613327026 acc 0.9428571462631226
saving 0.9428571462631226
sucess
