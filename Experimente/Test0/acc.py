import os
import csv
import numpy as np

with open('performance.csv') as performance:
    csv_reader = csv.reader(performance, delimiter=',')
    line_count = 0
    acc = []
    loss = []
    for row in csv_reader:
        if (line_count % 2) == 1:

            acc.append(float(row[4]))
        line_count += 1
        # loss.append(row[2])
acc = np.array(acc)
mean = np.mean(acc, dtype=np.float64)
var = np.var(acc, dtype=np.float64)
std = np.std(acc, dtype=np.float64)
print(acc, mean, var, std)
