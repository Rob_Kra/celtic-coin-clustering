Num GPUs Available:  1
Best acc:  0.8571428656578064
Found 293 files belonging to 11 classes.
Using 235 files for training.
Found 293 files belonging to 11 classes.
Using 58 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
2/2 - 2s - loss: 2.3398 - sparse_categorical_accuracy: 0.2340 - val_loss: 2.2170 - val_sparse_categorical_accuracy: 0.3276
Epoch 2/5
2/2 - 1s - loss: 1.7976 - sparse_categorical_accuracy: 0.8723 - val_loss: 2.1411 - val_sparse_categorical_accuracy: 0.3793
Epoch 3/5
2/2 - 1s - loss: 1.5764 - sparse_categorical_accuracy: 0.9957 - val_loss: 2.2061 - val_sparse_categorical_accuracy: 0.3103
Epoch 4/5
2/2 - 1s - loss: 1.5489 - sparse_categorical_accuracy: 1.0000 - val_loss: 2.2220 - val_sparse_categorical_accuracy: 0.3276
Epoch 5/5
2/2 - 1s - loss: 1.5441 - sparse_categorical_accuracy: 1.0000 - val_loss: 2.0946 - val_sparse_categorical_accuracy: 0.4483
35/35 - 0s - loss: 2.1633 - sparse_categorical_accuracy: 0.3714
ResNet_Test0 loss 2.163269281387329 acc 0.37142857909202576
Epoch 1/100
2/2 - 1s - loss: 1.5435 - sparse_categorical_accuracy: 1.0000 - val_loss: 2.0818 - val_sparse_categorical_accuracy: 0.4483
Epoch 2/100
2/2 - 1s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 2.0692 - val_sparse_categorical_accuracy: 0.4483
Epoch 3/100
2/2 - 1s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 2.0568 - val_sparse_categorical_accuracy: 0.4655
Epoch 4/100
2/2 - 1s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 2.0455 - val_sparse_categorical_accuracy: 0.4828
Epoch 5/100
2/2 - 1s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 2.0355 - val_sparse_categorical_accuracy: 0.4828
Epoch 6/100
2/2 - 1s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 2.0258 - val_sparse_categorical_accuracy: 0.5000
Epoch 7/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 2.0167 - val_sparse_categorical_accuracy: 0.5172
Epoch 8/100
2/2 - 1s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 2.0081 - val_sparse_categorical_accuracy: 0.5345
Epoch 9/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9988 - val_sparse_categorical_accuracy: 0.5345
Epoch 10/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9896 - val_sparse_categorical_accuracy: 0.5690
Epoch 11/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9804 - val_sparse_categorical_accuracy: 0.5690
Epoch 12/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9711 - val_sparse_categorical_accuracy: 0.5690
Epoch 13/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9621 - val_sparse_categorical_accuracy: 0.5862
Epoch 14/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9533 - val_sparse_categorical_accuracy: 0.6034
Epoch 15/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9448 - val_sparse_categorical_accuracy: 0.6034
Epoch 16/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9368 - val_sparse_categorical_accuracy: 0.6034
Epoch 17/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9293 - val_sparse_categorical_accuracy: 0.6207
Epoch 18/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9217 - val_sparse_categorical_accuracy: 0.6379
Epoch 19/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9141 - val_sparse_categorical_accuracy: 0.6379
Epoch 20/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9066 - val_sparse_categorical_accuracy: 0.6379
Epoch 21/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8992 - val_sparse_categorical_accuracy: 0.6379
Epoch 22/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8917 - val_sparse_categorical_accuracy: 0.6379
Epoch 23/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8843 - val_sparse_categorical_accuracy: 0.6379
Epoch 24/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8767 - val_sparse_categorical_accuracy: 0.6379
Epoch 25/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8692 - val_sparse_categorical_accuracy: 0.6724
Epoch 26/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8620 - val_sparse_categorical_accuracy: 0.6897
Epoch 27/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8549 - val_sparse_categorical_accuracy: 0.6897
Epoch 28/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8482 - val_sparse_categorical_accuracy: 0.7069
Epoch 29/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8420 - val_sparse_categorical_accuracy: 0.7069
Epoch 30/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8362 - val_sparse_categorical_accuracy: 0.7069
Epoch 31/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8306 - val_sparse_categorical_accuracy: 0.7241
Epoch 32/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8254 - val_sparse_categorical_accuracy: 0.7241
Epoch 33/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8207 - val_sparse_categorical_accuracy: 0.7586
Epoch 34/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8164 - val_sparse_categorical_accuracy: 0.7414
Epoch 35/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8127 - val_sparse_categorical_accuracy: 0.7586
Epoch 36/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8095 - val_sparse_categorical_accuracy: 0.7586
Epoch 37/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8066 - val_sparse_categorical_accuracy: 0.7759
Epoch 38/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8038 - val_sparse_categorical_accuracy: 0.7759
Epoch 39/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8012 - val_sparse_categorical_accuracy: 0.7759
Epoch 40/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7988 - val_sparse_categorical_accuracy: 0.7759
Epoch 41/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7967 - val_sparse_categorical_accuracy: 0.7759
Epoch 42/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7946 - val_sparse_categorical_accuracy: 0.7759
Epoch 43/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7928 - val_sparse_categorical_accuracy: 0.7759
Epoch 44/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7910 - val_sparse_categorical_accuracy: 0.7931
Epoch 45/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7894 - val_sparse_categorical_accuracy: 0.7759
Epoch 46/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7880 - val_sparse_categorical_accuracy: 0.7759
Epoch 47/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7866 - val_sparse_categorical_accuracy: 0.7759
Epoch 48/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7852 - val_sparse_categorical_accuracy: 0.7759
Epoch 49/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7839 - val_sparse_categorical_accuracy: 0.7759
Epoch 50/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7826 - val_sparse_categorical_accuracy: 0.7759
Epoch 51/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7815 - val_sparse_categorical_accuracy: 0.7759
Epoch 52/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7804 - val_sparse_categorical_accuracy: 0.7759
Epoch 53/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7794 - val_sparse_categorical_accuracy: 0.7759
Epoch 54/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7784 - val_sparse_categorical_accuracy: 0.7759
Epoch 55/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7776 - val_sparse_categorical_accuracy: 0.7759
Epoch 56/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7768 - val_sparse_categorical_accuracy: 0.7759
Epoch 57/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7761 - val_sparse_categorical_accuracy: 0.7759
Epoch 58/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7754 - val_sparse_categorical_accuracy: 0.7759
Epoch 59/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7749 - val_sparse_categorical_accuracy: 0.7759
Epoch 60/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7743 - val_sparse_categorical_accuracy: 0.7759
Epoch 61/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7738 - val_sparse_categorical_accuracy: 0.7759
Epoch 62/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7733 - val_sparse_categorical_accuracy: 0.7931
Epoch 63/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7728 - val_sparse_categorical_accuracy: 0.7931
Epoch 64/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7723 - val_sparse_categorical_accuracy: 0.7931
Epoch 65/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7718 - val_sparse_categorical_accuracy: 0.7931
Epoch 66/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7714 - val_sparse_categorical_accuracy: 0.7931
Epoch 67/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7711 - val_sparse_categorical_accuracy: 0.7931
Epoch 68/100
2/2 - 0s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7708 - val_sparse_categorical_accuracy: 0.7931
Epoch 69/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7704 - val_sparse_categorical_accuracy: 0.7931
Epoch 70/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7700 - val_sparse_categorical_accuracy: 0.7931
Epoch 71/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7697 - val_sparse_categorical_accuracy: 0.7931
Epoch 72/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7693 - val_sparse_categorical_accuracy: 0.7931
Epoch 73/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7690 - val_sparse_categorical_accuracy: 0.7931
Epoch 74/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7685 - val_sparse_categorical_accuracy: 0.7931
Epoch 75/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7681 - val_sparse_categorical_accuracy: 0.7931
Epoch 76/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7677 - val_sparse_categorical_accuracy: 0.7931
Epoch 77/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7674 - val_sparse_categorical_accuracy: 0.7931
Epoch 78/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7671 - val_sparse_categorical_accuracy: 0.7931
Epoch 79/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7668 - val_sparse_categorical_accuracy: 0.7931
Epoch 80/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7666 - val_sparse_categorical_accuracy: 0.7931
Epoch 81/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7663 - val_sparse_categorical_accuracy: 0.7931
Epoch 82/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7660 - val_sparse_categorical_accuracy: 0.7931
Epoch 83/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7657 - val_sparse_categorical_accuracy: 0.7931
Epoch 84/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7655 - val_sparse_categorical_accuracy: 0.7931
Epoch 85/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7652 - val_sparse_categorical_accuracy: 0.7931
Epoch 86/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7650 - val_sparse_categorical_accuracy: 0.7931
Epoch 87/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7647 - val_sparse_categorical_accuracy: 0.7759
Epoch 88/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7644 - val_sparse_categorical_accuracy: 0.7759
Epoch 89/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7641 - val_sparse_categorical_accuracy: 0.7759
Epoch 90/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7638 - val_sparse_categorical_accuracy: 0.7759
Epoch 91/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7635 - val_sparse_categorical_accuracy: 0.7759
Epoch 92/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7632 - val_sparse_categorical_accuracy: 0.7759
Epoch 93/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7629 - val_sparse_categorical_accuracy: 0.7759
Epoch 94/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7627 - val_sparse_categorical_accuracy: 0.7759
Epoch 95/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7624 - val_sparse_categorical_accuracy: 0.7759
Epoch 96/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7621 - val_sparse_categorical_accuracy: 0.7759
Epoch 97/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7616 - val_sparse_categorical_accuracy: 0.7759
Epoch 98/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7613 - val_sparse_categorical_accuracy: 0.7759
Epoch 99/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7609 - val_sparse_categorical_accuracy: 0.7931
Epoch 100/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7607 - val_sparse_categorical_accuracy: 0.7931
ResNet_Test0 loss 1.7167772054672241 acc 0.8857142925262451
saving 0.8857142925262451
sucess
