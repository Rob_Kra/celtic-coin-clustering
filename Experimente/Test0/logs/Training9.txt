Num GPUs Available:  1
Best acc:  0.8571428656578064
Found 293 files belonging to 11 classes.
Using 235 files for training.
Found 293 files belonging to 11 classes.
Using 58 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
2/2 - 2s - loss: 2.3322 - sparse_categorical_accuracy: 0.2340 - val_loss: 2.2761 - val_sparse_categorical_accuracy: 0.2414
Epoch 2/5
2/2 - 1s - loss: 1.7208 - sparse_categorical_accuracy: 0.9489 - val_loss: 2.0379 - val_sparse_categorical_accuracy: 0.5172
Epoch 3/5
2/2 - 1s - loss: 1.5591 - sparse_categorical_accuracy: 1.0000 - val_loss: 2.0422 - val_sparse_categorical_accuracy: 0.4828
Epoch 4/5
2/2 - 1s - loss: 1.5452 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9975 - val_sparse_categorical_accuracy: 0.5345
Epoch 5/5
2/2 - 1s - loss: 1.5439 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9838 - val_sparse_categorical_accuracy: 0.5517
35/35 - 0s - loss: 2.0376 - sparse_categorical_accuracy: 0.5143
ResNet_Test0 loss 2.037586212158203 acc 0.5142857432365417
Epoch 1/100
2/2 - 1s - loss: 1.5436 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9736 - val_sparse_categorical_accuracy: 0.5690
Epoch 2/100
2/2 - 1s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9643 - val_sparse_categorical_accuracy: 0.5690
Epoch 3/100
2/2 - 1s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9555 - val_sparse_categorical_accuracy: 0.5690
Epoch 4/100
2/2 - 1s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9459 - val_sparse_categorical_accuracy: 0.5862
Epoch 5/100
2/2 - 1s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9370 - val_sparse_categorical_accuracy: 0.6207
Epoch 6/100
2/2 - 1s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9290 - val_sparse_categorical_accuracy: 0.6379
Epoch 7/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9215 - val_sparse_categorical_accuracy: 0.6379
Epoch 8/100
2/2 - 1s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9140 - val_sparse_categorical_accuracy: 0.6379
Epoch 9/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9071 - val_sparse_categorical_accuracy: 0.6379
Epoch 10/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.9006 - val_sparse_categorical_accuracy: 0.6379
Epoch 11/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8943 - val_sparse_categorical_accuracy: 0.6552
Epoch 12/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8883 - val_sparse_categorical_accuracy: 0.6552
Epoch 13/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8829 - val_sparse_categorical_accuracy: 0.6552
Epoch 14/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8783 - val_sparse_categorical_accuracy: 0.6552
Epoch 15/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8739 - val_sparse_categorical_accuracy: 0.6724
Epoch 16/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8698 - val_sparse_categorical_accuracy: 0.6724
Epoch 17/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8660 - val_sparse_categorical_accuracy: 0.6724
Epoch 18/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8622 - val_sparse_categorical_accuracy: 0.6724
Epoch 19/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8585 - val_sparse_categorical_accuracy: 0.6724
Epoch 20/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8548 - val_sparse_categorical_accuracy: 0.6724
Epoch 21/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8512 - val_sparse_categorical_accuracy: 0.6897
Epoch 22/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8476 - val_sparse_categorical_accuracy: 0.6897
Epoch 23/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8437 - val_sparse_categorical_accuracy: 0.6897
Epoch 24/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8399 - val_sparse_categorical_accuracy: 0.6897
Epoch 25/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8363 - val_sparse_categorical_accuracy: 0.6897
Epoch 26/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8325 - val_sparse_categorical_accuracy: 0.6897
Epoch 27/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8288 - val_sparse_categorical_accuracy: 0.6897
Epoch 28/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8249 - val_sparse_categorical_accuracy: 0.6897
Epoch 29/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8209 - val_sparse_categorical_accuracy: 0.7241
Epoch 30/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8171 - val_sparse_categorical_accuracy: 0.7241
Epoch 31/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8136 - val_sparse_categorical_accuracy: 0.7241
Epoch 32/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8101 - val_sparse_categorical_accuracy: 0.7241
Epoch 33/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8066 - val_sparse_categorical_accuracy: 0.7241
Epoch 34/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8033 - val_sparse_categorical_accuracy: 0.7241
Epoch 35/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.8001 - val_sparse_categorical_accuracy: 0.7414
Epoch 36/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7971 - val_sparse_categorical_accuracy: 0.7414
Epoch 37/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7943 - val_sparse_categorical_accuracy: 0.7931
Epoch 38/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7916 - val_sparse_categorical_accuracy: 0.7931
Epoch 39/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7890 - val_sparse_categorical_accuracy: 0.7931
Epoch 40/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7866 - val_sparse_categorical_accuracy: 0.7931
Epoch 41/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7842 - val_sparse_categorical_accuracy: 0.7931
Epoch 42/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7821 - val_sparse_categorical_accuracy: 0.7931
Epoch 43/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7801 - val_sparse_categorical_accuracy: 0.7931
Epoch 44/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7782 - val_sparse_categorical_accuracy: 0.7931
Epoch 45/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7764 - val_sparse_categorical_accuracy: 0.7931
Epoch 46/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7748 - val_sparse_categorical_accuracy: 0.7759
Epoch 47/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7733 - val_sparse_categorical_accuracy: 0.7931
Epoch 48/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7719 - val_sparse_categorical_accuracy: 0.7759
Epoch 49/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7705 - val_sparse_categorical_accuracy: 0.7586
Epoch 50/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7691 - val_sparse_categorical_accuracy: 0.7586
Epoch 51/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7677 - val_sparse_categorical_accuracy: 0.7586
Epoch 52/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7665 - val_sparse_categorical_accuracy: 0.7586
Epoch 53/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7653 - val_sparse_categorical_accuracy: 0.7586
Epoch 54/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7640 - val_sparse_categorical_accuracy: 0.7586
Epoch 55/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7628 - val_sparse_categorical_accuracy: 0.7586
Epoch 56/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7616 - val_sparse_categorical_accuracy: 0.7586
Epoch 57/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7603 - val_sparse_categorical_accuracy: 0.7586
Epoch 58/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7592 - val_sparse_categorical_accuracy: 0.7586
Epoch 59/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7579 - val_sparse_categorical_accuracy: 0.7586
Epoch 60/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7567 - val_sparse_categorical_accuracy: 0.7586
Epoch 61/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7556 - val_sparse_categorical_accuracy: 0.7586
Epoch 62/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7543 - val_sparse_categorical_accuracy: 0.7586
Epoch 63/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7530 - val_sparse_categorical_accuracy: 0.7586
Epoch 64/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7517 - val_sparse_categorical_accuracy: 0.7586
Epoch 65/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7504 - val_sparse_categorical_accuracy: 0.7759
Epoch 66/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7492 - val_sparse_categorical_accuracy: 0.7759
Epoch 67/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7478 - val_sparse_categorical_accuracy: 0.7931
Epoch 68/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7465 - val_sparse_categorical_accuracy: 0.7931
Epoch 69/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7454 - val_sparse_categorical_accuracy: 0.7931
Epoch 70/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7442 - val_sparse_categorical_accuracy: 0.7931
Epoch 71/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7431 - val_sparse_categorical_accuracy: 0.8103
Epoch 72/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7421 - val_sparse_categorical_accuracy: 0.8103
Epoch 73/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7410 - val_sparse_categorical_accuracy: 0.8103
Epoch 74/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7400 - val_sparse_categorical_accuracy: 0.8103
Epoch 75/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7390 - val_sparse_categorical_accuracy: 0.8276
Epoch 76/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7381 - val_sparse_categorical_accuracy: 0.8276
Epoch 77/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7372 - val_sparse_categorical_accuracy: 0.8276
Epoch 78/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7364 - val_sparse_categorical_accuracy: 0.8276
Epoch 79/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7356 - val_sparse_categorical_accuracy: 0.8276
Epoch 80/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7349 - val_sparse_categorical_accuracy: 0.8276
Epoch 81/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7342 - val_sparse_categorical_accuracy: 0.8276
Epoch 82/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7335 - val_sparse_categorical_accuracy: 0.8276
Epoch 83/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7328 - val_sparse_categorical_accuracy: 0.8276
Epoch 84/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7322 - val_sparse_categorical_accuracy: 0.8276
Epoch 85/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7316 - val_sparse_categorical_accuracy: 0.8276
Epoch 86/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7311 - val_sparse_categorical_accuracy: 0.8276
Epoch 87/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7305 - val_sparse_categorical_accuracy: 0.8276
Epoch 88/100
2/2 - 0s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7300 - val_sparse_categorical_accuracy: 0.8276
Epoch 89/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7294 - val_sparse_categorical_accuracy: 0.8276
Epoch 90/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7290 - val_sparse_categorical_accuracy: 0.8276
Epoch 91/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7285 - val_sparse_categorical_accuracy: 0.8276
Epoch 92/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7281 - val_sparse_categorical_accuracy: 0.8276
Epoch 93/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7277 - val_sparse_categorical_accuracy: 0.8276
Epoch 94/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7273 - val_sparse_categorical_accuracy: 0.8276
Epoch 95/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7269 - val_sparse_categorical_accuracy: 0.8276
Epoch 96/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7265 - val_sparse_categorical_accuracy: 0.8276
Epoch 97/100
2/2 - 1s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7262 - val_sparse_categorical_accuracy: 0.8276
Epoch 98/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7258 - val_sparse_categorical_accuracy: 0.8276
Epoch 99/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7254 - val_sparse_categorical_accuracy: 0.8276
Epoch 100/100
2/2 - 1s - loss: 1.5430 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.7250 - val_sparse_categorical_accuracy: 0.8276
ResNet_Test0 loss 1.7391518354415894 acc 0.8285714387893677
sucess
