Num GPUs Available:  1
Best acc:  0.9142857193946838
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7170 - sparse_categorical_accuracy: 0.8448 - val_loss: 1.9322 - val_sparse_categorical_accuracy: 0.6099
Epoch 2/5
36/36 - 23s - loss: 1.5641 - sparse_categorical_accuracy: 0.9823 - val_loss: 1.7720 - val_sparse_categorical_accuracy: 0.7704
Epoch 3/5
36/36 - 23s - loss: 1.5555 - sparse_categorical_accuracy: 0.9898 - val_loss: 1.5727 - val_sparse_categorical_accuracy: 0.9716
Epoch 4/5
36/36 - 23s - loss: 1.5501 - sparse_categorical_accuracy: 0.9938 - val_loss: 1.5819 - val_sparse_categorical_accuracy: 0.9610
Epoch 5/5
36/36 - 23s - loss: 1.5491 - sparse_categorical_accuracy: 0.9947 - val_loss: 1.5751 - val_sparse_categorical_accuracy: 0.9681
35/35 - 0s - loss: 1.7404 - sparse_categorical_accuracy: 0.8286
ResNet_Test4 loss 1.7404022216796875 acc 0.8285714387893677
Epoch 1/100
36/36 - 24s - loss: 1.5467 - sparse_categorical_accuracy: 0.9969 - val_loss: 1.5561 - val_sparse_categorical_accuracy: 0.9867
Epoch 2/100
36/36 - 24s - loss: 1.5446 - sparse_categorical_accuracy: 0.9989 - val_loss: 1.5498 - val_sparse_categorical_accuracy: 0.9929
Epoch 3/100
36/36 - 24s - loss: 1.5439 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5466 - val_sparse_categorical_accuracy: 0.9965
Epoch 4/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5454 - val_sparse_categorical_accuracy: 0.9982
Epoch 5/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 6/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 1.0000
Epoch 7/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5438 - val_sparse_categorical_accuracy: 1.0000
Epoch 8/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5436 - val_sparse_categorical_accuracy: 1.0000
Epoch 9/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5435 - val_sparse_categorical_accuracy: 1.0000
Epoch 10/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5436 - val_sparse_categorical_accuracy: 1.0000
Epoch 11/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5435 - val_sparse_categorical_accuracy: 1.0000
Epoch 12/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5434 - val_sparse_categorical_accuracy: 1.0000
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5434 - val_sparse_categorical_accuracy: 1.0000
Epoch 14/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5433 - val_sparse_categorical_accuracy: 1.0000
Epoch 15/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5433 - val_sparse_categorical_accuracy: 1.0000
Epoch 16/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5433 - val_sparse_categorical_accuracy: 1.0000
ResNet_Test4 loss 1.6871342658996582 acc 0.8571428656578064
sucess
