Num GPUs Available:  1
Best acc:  0.9142857193946838
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7284 - sparse_categorical_accuracy: 0.8330 - val_loss: 2.0647 - val_sparse_categorical_accuracy: 0.4770
Epoch 2/5
36/36 - 24s - loss: 1.5674 - sparse_categorical_accuracy: 0.9787 - val_loss: 1.7631 - val_sparse_categorical_accuracy: 0.7793
Epoch 3/5
36/36 - 23s - loss: 1.5555 - sparse_categorical_accuracy: 0.9907 - val_loss: 1.6203 - val_sparse_categorical_accuracy: 0.9220
Epoch 4/5
36/36 - 23s - loss: 1.5574 - sparse_categorical_accuracy: 0.9874 - val_loss: 1.6290 - val_sparse_categorical_accuracy: 0.9140
Epoch 5/5
36/36 - 23s - loss: 1.5500 - sparse_categorical_accuracy: 0.9940 - val_loss: 1.6552 - val_sparse_categorical_accuracy: 0.8865
35/35 - 0s - loss: 1.8255 - sparse_categorical_accuracy: 0.7143
ResNet_Test4 loss 1.8254685401916504 acc 0.7142857313156128
Epoch 1/100
36/36 - 24s - loss: 1.5450 - sparse_categorical_accuracy: 0.9991 - val_loss: 1.5684 - val_sparse_categorical_accuracy: 0.9734
Epoch 2/100
36/36 - 24s - loss: 1.5436 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5527 - val_sparse_categorical_accuracy: 0.9902
Epoch 3/100
36/36 - 24s - loss: 1.5434 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5492 - val_sparse_categorical_accuracy: 0.9938
Epoch 4/100
36/36 - 24s - loss: 1.5435 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5470 - val_sparse_categorical_accuracy: 0.9965
Epoch 5/100
36/36 - 24s - loss: 1.5434 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9982
Epoch 6/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5455 - val_sparse_categorical_accuracy: 0.9982
Epoch 7/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9982
Epoch 8/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
Epoch 9/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
Epoch 10/100
36/36 - 23s - loss: 1.5435 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 11/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 12/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 14/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
Epoch 15/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 16/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 17/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9991
Epoch 18/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
Epoch 19/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 20/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9982
ResNet_Test4 loss 1.6401220560073853 acc 0.8857142925262451
sucess
