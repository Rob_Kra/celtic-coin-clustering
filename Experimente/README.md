### Experimente

*Experimente* contains trained CNNs of the experiments from my bachelor thesis and also the extracted features for the clustering.

*ResNet_Cluster36.h5* was the CNN used for the clustering. (´create_KMeansClustering(n_clusters=36, n_init=50, max_iter=2000,random_state=3 )´ are the exact parameters.

*Agglo_overview.csv* and *Kmeans_overview.csv* are the overviews in tabular form.

Also there are two overviews of the Kmeans and Hierarchical Clustering.

The *Test(0-9)* folders contain the scripts for data augmentation, log files of the training, performance files and  saved CNNs.