Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7298 - sparse_categorical_accuracy: 0.8328 - val_loss: 1.8487 - val_sparse_categorical_accuracy: 0.6986
Epoch 2/5
36/36 - 23s - loss: 1.5665 - sparse_categorical_accuracy: 0.9787 - val_loss: 1.9226 - val_sparse_categorical_accuracy: 0.6144
Epoch 3/5
36/36 - 23s - loss: 1.5603 - sparse_categorical_accuracy: 0.9852 - val_loss: 1.6374 - val_sparse_categorical_accuracy: 0.9060
Epoch 4/5
36/36 - 23s - loss: 1.5515 - sparse_categorical_accuracy: 0.9925 - val_loss: 1.5762 - val_sparse_categorical_accuracy: 0.9672
Epoch 5/5
36/36 - 23s - loss: 1.5514 - sparse_categorical_accuracy: 0.9920 - val_loss: 1.6125 - val_sparse_categorical_accuracy: 0.9309
35/35 - 0s - loss: 1.6980 - sparse_categorical_accuracy: 0.8286
ResNet_Test1 loss 1.697996735572815 acc 0.8285714387893677
Epoch 1/100
36/36 - 24s - loss: 1.5471 - sparse_categorical_accuracy: 0.9962 - val_loss: 1.5623 - val_sparse_categorical_accuracy: 0.9805
Epoch 2/100
36/36 - 24s - loss: 1.5443 - sparse_categorical_accuracy: 0.9991 - val_loss: 1.5497 - val_sparse_categorical_accuracy: 0.9947
Epoch 3/100
36/36 - 23s - loss: 1.5444 - sparse_categorical_accuracy: 0.9989 - val_loss: 1.5474 - val_sparse_categorical_accuracy: 0.9965
Epoch 4/100
36/36 - 23s - loss: 1.5440 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5463 - val_sparse_categorical_accuracy: 0.9965
Epoch 5/100
36/36 - 23s - loss: 1.5437 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5456 - val_sparse_categorical_accuracy: 0.9973
Epoch 6/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 7/100
36/36 - 24s - loss: 1.5436 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9991
Epoch 8/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 9/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 10/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 11/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 12/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5443 - val_sparse_categorical_accuracy: 0.9991
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5443 - val_sparse_categorical_accuracy: 0.9991
Epoch 14/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5443 - val_sparse_categorical_accuracy: 0.9991
Epoch 15/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 16/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
ResNet_Test1 loss 1.7062329053878784 acc 0.8285714387893677
sucess
