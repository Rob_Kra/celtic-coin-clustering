Num GPUs Available:  1
Best acc:  0
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.

Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7338 - sparse_categorical_accuracy: 0.8235 - val_loss: 1.8074 - val_sparse_categorical_accuracy: 0.7296
Epoch 2/5
36/36 - 25s - loss: 1.5715 - sparse_categorical_accuracy: 0.9745 - val_loss: 1.7500 - val_sparse_categorical_accuracy: 0.7952
Epoch 3/5
36/36 - 23s - loss: 1.5622 - sparse_categorical_accuracy: 0.9836 - val_loss: 1.6355 - val_sparse_categorical_accuracy: 0.9096
Epoch 4/5
36/36 - 23s - loss: 1.5573 - sparse_categorical_accuracy: 0.9876 - val_loss: 2.0444 - val_sparse_categorical_accuracy: 0.4973
Epoch 5/5
36/36 - 23s - loss: 1.5497 - sparse_categorical_accuracy: 0.9938 - val_loss: 1.6063 - val_sparse_categorical_accuracy: 0.9379
35/35 - 0s - loss: 1.8257 - sparse_categorical_accuracy: 0.7143
ResNet_Test1 loss 1.8257238864898682 acc 0.7142857313156128
Epoch 1/100
36/36 - 24s - loss: 1.5458 - sparse_categorical_accuracy: 0.9982 - val_loss: 1.5624 - val_sparse_categorical_accuracy: 0.9814
Epoch 2/100
36/36 - 23s - loss: 1.5438 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5520 - val_sparse_categorical_accuracy: 0.9929
Epoch 3/100
36/36 - 24s - loss: 1.5435 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5497 - val_sparse_categorical_accuracy: 0.9938
Epoch 4/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5479 - val_sparse_categorical_accuracy: 0.9956
Epoch 5/100
36/36 - 24s - loss: 1.5435 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5466 - val_sparse_categorical_accuracy: 0.9973
Epoch 6/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5460 - val_sparse_categorical_accuracy: 0.9982
Epoch 7/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5456 - val_sparse_categorical_accuracy: 0.9982
Epoch 8/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5454 - val_sparse_categorical_accuracy: 0.9982
Epoch 9/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9982
Epoch 10/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9991
Epoch 11/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9991
Epoch 12/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9982
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 14/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 15/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9982
Epoch 16/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9982
Epoch 17/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 18/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
ResNet_Test1 loss 1.6295766830444336 acc 0.9142857193946838
saving 0.9142857193946838
sucess
