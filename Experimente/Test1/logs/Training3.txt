Num GPUs Available:  1
Best acc:  0.9142857193946838
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7413 - sparse_categorical_accuracy: 0.8171 - val_loss: 1.7389 - val_sparse_categorical_accuracy: 0.8174
Epoch 2/5
36/36 - 23s - loss: 1.5782 - sparse_categorical_accuracy: 0.9686 - val_loss: 1.7103 - val_sparse_categorical_accuracy: 0.8333
Epoch 3/5
36/36 - 23s - loss: 1.5566 - sparse_categorical_accuracy: 0.9889 - val_loss: 1.6172 - val_sparse_categorical_accuracy: 0.9273
Epoch 4/5
36/36 - 24s - loss: 1.5533 - sparse_categorical_accuracy: 0.9905 - val_loss: 1.5849 - val_sparse_categorical_accuracy: 0.9592
Epoch 5/5
36/36 - 24s - loss: 1.5505 - sparse_categorical_accuracy: 0.9927 - val_loss: 1.5653 - val_sparse_categorical_accuracy: 0.9778
35/35 - 0s - loss: 1.6287 - sparse_categorical_accuracy: 0.9143
ResNet_Test1 loss 1.6286993026733398 acc 0.9142857193946838
Epoch 1/100
36/36 - 25s - loss: 1.5469 - sparse_categorical_accuracy: 0.9969 - val_loss: 1.5548 - val_sparse_categorical_accuracy: 0.9894
Epoch 2/100
36/36 - 24s - loss: 1.5456 - sparse_categorical_accuracy: 0.9976 - val_loss: 1.5505 - val_sparse_categorical_accuracy: 0.9920
Epoch 3/100
36/36 - 24s - loss: 1.5439 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5487 - val_sparse_categorical_accuracy: 0.9929
Epoch 4/100
36/36 - 24s - loss: 1.5437 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5472 - val_sparse_categorical_accuracy: 0.9973
Epoch 5/100
36/36 - 23s - loss: 1.5438 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5465 - val_sparse_categorical_accuracy: 0.9973
Epoch 6/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5464 - val_sparse_categorical_accuracy: 0.9973
Epoch 7/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5463 - val_sparse_categorical_accuracy: 0.9973
Epoch 8/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5461 - val_sparse_categorical_accuracy: 0.9973
Epoch 9/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9982
Epoch 10/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5457 - val_sparse_categorical_accuracy: 0.9982
Epoch 11/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5454 - val_sparse_categorical_accuracy: 0.9982
Epoch 12/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9982
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 14/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9991
Epoch 15/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9991
Epoch 16/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9991
Epoch 17/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9991
Epoch 18/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9991
Epoch 19/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9991
Epoch 20/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9991
Epoch 21/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 22/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 23/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5443 - val_sparse_categorical_accuracy: 0.9991
Epoch 24/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 1.0000
ResNet_Test1 loss 1.6148720979690552 acc 0.9428571462631226
saving 0.9428571462631226
sucess
