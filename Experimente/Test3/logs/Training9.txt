Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 24s - loss: 1.7451 - sparse_categorical_accuracy: 0.8085 - val_loss: 1.9786 - val_sparse_categorical_accuracy: 0.5585
Epoch 2/5
36/36 - 23s - loss: 1.5659 - sparse_categorical_accuracy: 0.9801 - val_loss: 1.7748 - val_sparse_categorical_accuracy: 0.7677
Epoch 3/5
36/36 - 22s - loss: 1.5529 - sparse_categorical_accuracy: 0.9922 - val_loss: 1.6020 - val_sparse_categorical_accuracy: 0.9415
Epoch 4/5
36/36 - 22s - loss: 1.5517 - sparse_categorical_accuracy: 0.9918 - val_loss: 1.6037 - val_sparse_categorical_accuracy: 0.9406
Epoch 5/5
36/36 - 23s - loss: 1.5553 - sparse_categorical_accuracy: 0.9900 - val_loss: 1.5937 - val_sparse_categorical_accuracy: 0.9495
35/35 - 0s - loss: 1.7537 - sparse_categorical_accuracy: 0.8000
ResNet_Test3 loss 1.7537457942962646 acc 0.800000011920929
Epoch 1/100
36/36 - 23s - loss: 1.5519 - sparse_categorical_accuracy: 0.9927 - val_loss: 1.5672 - val_sparse_categorical_accuracy: 0.9770
Epoch 2/100
36/36 - 23s - loss: 1.5460 - sparse_categorical_accuracy: 0.9982 - val_loss: 1.5529 - val_sparse_categorical_accuracy: 0.9911
Epoch 3/100
36/36 - 23s - loss: 1.5447 - sparse_categorical_accuracy: 0.9991 - val_loss: 1.5473 - val_sparse_categorical_accuracy: 0.9973
Epoch 4/100
36/36 - 23s - loss: 1.5437 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5457 - val_sparse_categorical_accuracy: 0.9991
Epoch 5/100
36/36 - 22s - loss: 1.5437 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9991
Epoch 6/100
36/36 - 24s - loss: 1.5434 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9991
Epoch 7/100
36/36 - 23s - loss: 1.5435 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9991
Epoch 8/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9991
Epoch 9/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9991
Epoch 10/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9991
Epoch 11/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9991
Epoch 12/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9991
Epoch 13/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9991
Epoch 14/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9991
Epoch 15/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 16/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 17/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 18/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 19/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 20/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 21/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 22/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 23/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
ResNet_Test3 loss 1.6343257427215576 acc 0.9142857193946838
sucess
