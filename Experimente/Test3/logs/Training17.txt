Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 24s - loss: 1.7421 - sparse_categorical_accuracy: 0.8173 - val_loss: 1.9072 - val_sparse_categorical_accuracy: 0.6321
Epoch 2/5
36/36 - 23s - loss: 1.5853 - sparse_categorical_accuracy: 0.9612 - val_loss: 1.6792 - val_sparse_categorical_accuracy: 0.8626
Epoch 3/5
36/36 - 23s - loss: 1.5622 - sparse_categorical_accuracy: 0.9832 - val_loss: 1.6272 - val_sparse_categorical_accuracy: 0.9158
Epoch 4/5
36/36 - 23s - loss: 1.5513 - sparse_categorical_accuracy: 0.9925 - val_loss: 1.5832 - val_sparse_categorical_accuracy: 0.9601
Epoch 5/5
36/36 - 23s - loss: 1.5533 - sparse_categorical_accuracy: 0.9903 - val_loss: 1.5673 - val_sparse_categorical_accuracy: 0.9752
35/35 - 0s - loss: 1.6073 - sparse_categorical_accuracy: 0.9429
ResNet_Test3 loss 1.6072779893875122 acc 0.9428571462631226
Epoch 1/100
36/36 - 24s - loss: 1.5461 - sparse_categorical_accuracy: 0.9978 - val_loss: 1.5568 - val_sparse_categorical_accuracy: 0.9867
Epoch 2/100
36/36 - 24s - loss: 1.5445 - sparse_categorical_accuracy: 0.9987 - val_loss: 1.5523 - val_sparse_categorical_accuracy: 0.9902
Epoch 3/100
36/36 - 23s - loss: 1.5439 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5504 - val_sparse_categorical_accuracy: 0.9920
Epoch 4/100
36/36 - 24s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5494 - val_sparse_categorical_accuracy: 0.9920
Epoch 5/100
36/36 - 25s - loss: 1.5436 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5480 - val_sparse_categorical_accuracy: 0.9956
Epoch 6/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5472 - val_sparse_categorical_accuracy: 0.9965
Epoch 7/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5468 - val_sparse_categorical_accuracy: 0.9965
Epoch 8/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5463 - val_sparse_categorical_accuracy: 0.9973
Epoch 9/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5461 - val_sparse_categorical_accuracy: 0.9973
Epoch 10/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5459 - val_sparse_categorical_accuracy: 0.9973
Epoch 11/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5457 - val_sparse_categorical_accuracy: 0.9982
Epoch 12/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5456 - val_sparse_categorical_accuracy: 0.9982
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5453 - val_sparse_categorical_accuracy: 0.9982
Epoch 14/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5453 - val_sparse_categorical_accuracy: 0.9982
Epoch 15/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5453 - val_sparse_categorical_accuracy: 0.9982
Epoch 16/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 17/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 18/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 19/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 20/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
Epoch 21/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
ResNet_Test3 loss 1.604507327079773 acc 0.9428571462631226
sucess
