Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7688 - sparse_categorical_accuracy: 0.7923 - val_loss: 2.0446 - val_sparse_categorical_accuracy: 0.4991
Epoch 2/5
36/36 - 23s - loss: 1.5815 - sparse_categorical_accuracy: 0.9659 - val_loss: 1.6959 - val_sparse_categorical_accuracy: 0.8449
Epoch 3/5
36/36 - 23s - loss: 1.5647 - sparse_categorical_accuracy: 0.9803 - val_loss: 1.6225 - val_sparse_categorical_accuracy: 0.9238
Epoch 4/5
36/36 - 23s - loss: 1.5582 - sparse_categorical_accuracy: 0.9860 - val_loss: 1.5831 - val_sparse_categorical_accuracy: 0.9637
Epoch 5/5
36/36 - 23s - loss: 1.5550 - sparse_categorical_accuracy: 0.9891 - val_loss: 1.6455 - val_sparse_categorical_accuracy: 0.8954
35/35 - 0s - loss: 1.9383 - sparse_categorical_accuracy: 0.6000
ResNet_Test14 loss 1.938289761543274 acc 0.6000000238418579
Epoch 1/100
36/36 - 24s - loss: 1.5468 - sparse_categorical_accuracy: 0.9967 - val_loss: 1.5654 - val_sparse_categorical_accuracy: 0.9796
Epoch 2/100
36/36 - 24s - loss: 1.5457 - sparse_categorical_accuracy: 0.9980 - val_loss: 1.5536 - val_sparse_categorical_accuracy: 0.9911
Epoch 3/100
36/36 - 24s - loss: 1.5445 - sparse_categorical_accuracy: 0.9989 - val_loss: 1.5519 - val_sparse_categorical_accuracy: 0.9920
Epoch 4/100
36/36 - 23s - loss: 1.5440 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5509 - val_sparse_categorical_accuracy: 0.9929
Epoch 5/100
36/36 - 23s - loss: 1.5438 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5502 - val_sparse_categorical_accuracy: 0.9929
Epoch 6/100
36/36 - 24s - loss: 1.5435 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5496 - val_sparse_categorical_accuracy: 0.9947
Epoch 7/100
36/36 - 23s - loss: 1.5435 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5492 - val_sparse_categorical_accuracy: 0.9947
Epoch 8/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5486 - val_sparse_categorical_accuracy: 0.9956
Epoch 9/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5482 - val_sparse_categorical_accuracy: 0.9956
Epoch 10/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5477 - val_sparse_categorical_accuracy: 0.9965
Epoch 11/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5475 - val_sparse_categorical_accuracy: 0.9973
Epoch 12/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5474 - val_sparse_categorical_accuracy: 0.9982
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5473 - val_sparse_categorical_accuracy: 0.9982
Epoch 14/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5472 - val_sparse_categorical_accuracy: 0.9982
Epoch 15/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5472 - val_sparse_categorical_accuracy: 0.9982
Epoch 16/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5472 - val_sparse_categorical_accuracy: 0.9973
Epoch 17/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5472 - val_sparse_categorical_accuracy: 0.9973
Epoch 18/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5472 - val_sparse_categorical_accuracy: 0.9982
Epoch 19/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5470 - val_sparse_categorical_accuracy: 0.9982
Epoch 20/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5470 - val_sparse_categorical_accuracy: 0.9982
Epoch 21/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5469 - val_sparse_categorical_accuracy: 0.9982
ResNet_Test14 loss 1.6863298416137695 acc 0.8571428656578064
sucess
