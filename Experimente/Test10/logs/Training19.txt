Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.8222 - sparse_categorical_accuracy: 0.7343 - val_loss: 1.8959 - val_sparse_categorical_accuracy: 0.6498
Epoch 2/5
36/36 - 23s - loss: 1.5993 - sparse_categorical_accuracy: 0.9473 - val_loss: 1.9279 - val_sparse_categorical_accuracy: 0.6144
Epoch 3/5
36/36 - 23s - loss: 1.5683 - sparse_categorical_accuracy: 0.9787 - val_loss: 1.6580 - val_sparse_categorical_accuracy: 0.8848
Epoch 4/5
36/36 - 23s - loss: 1.5556 - sparse_categorical_accuracy: 0.9891 - val_loss: 1.6108 - val_sparse_categorical_accuracy: 0.9326
Epoch 5/5
36/36 - 23s - loss: 1.5491 - sparse_categorical_accuracy: 0.9949 - val_loss: 1.5791 - val_sparse_categorical_accuracy: 0.9628
35/35 - 0s - loss: 1.6222 - sparse_categorical_accuracy: 0.9429
ResNet_Test14 loss 1.6221636533737183 acc 0.9428571462631226
Epoch 1/100
36/36 - 23s - loss: 1.5444 - sparse_categorical_accuracy: 0.9989 - val_loss: 1.5523 - val_sparse_categorical_accuracy: 0.9911
Epoch 2/100
36/36 - 24s - loss: 1.5438 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5464 - val_sparse_categorical_accuracy: 0.9982
Epoch 3/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5453 - val_sparse_categorical_accuracy: 0.9982
Epoch 4/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 5/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
Epoch 6/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 7/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 8/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
Epoch 9/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
Epoch 10/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 11/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9982
Epoch 12/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9982
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9982
ResNet_Test14 loss 1.657452940940857 acc 0.8857142925262451
sucess
