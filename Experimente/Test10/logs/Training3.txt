Num GPUs Available:  1
Best acc:  0.8571428656578064
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7876 - sparse_categorical_accuracy: 0.7706 - val_loss: 1.8277 - val_sparse_categorical_accuracy: 0.7145
Epoch 2/5
36/36 - 23s - loss: 1.5772 - sparse_categorical_accuracy: 0.9705 - val_loss: 1.7504 - val_sparse_categorical_accuracy: 0.7926
Epoch 3/5
36/36 - 23s - loss: 1.5720 - sparse_categorical_accuracy: 0.9734 - val_loss: 1.6824 - val_sparse_categorical_accuracy: 0.8626
Epoch 4/5
36/36 - 23s - loss: 1.5503 - sparse_categorical_accuracy: 0.9947 - val_loss: 1.5662 - val_sparse_categorical_accuracy: 0.9778
Epoch 5/5
36/36 - 23s - loss: 1.5538 - sparse_categorical_accuracy: 0.9903 - val_loss: 1.5978 - val_sparse_categorical_accuracy: 0.9468
35/35 - 0s - loss: 1.7670 - sparse_categorical_accuracy: 0.8000
ResNet_Test14 loss 1.7669553756713867 acc 0.800000011920929
Epoch 1/100
36/36 - 24s - loss: 1.5490 - sparse_categorical_accuracy: 0.9947 - val_loss: 1.5702 - val_sparse_categorical_accuracy: 0.9716
Epoch 2/100
36/36 - 24s - loss: 1.5466 - sparse_categorical_accuracy: 0.9976 - val_loss: 1.5576 - val_sparse_categorical_accuracy: 0.9858
Epoch 3/100
36/36 - 24s - loss: 1.5445 - sparse_categorical_accuracy: 0.9991 - val_loss: 1.5560 - val_sparse_categorical_accuracy: 0.9867
Epoch 4/100
36/36 - 24s - loss: 1.5446 - sparse_categorical_accuracy: 0.9987 - val_loss: 1.5543 - val_sparse_categorical_accuracy: 0.9902
Epoch 5/100
36/36 - 23s - loss: 1.5435 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5536 - val_sparse_categorical_accuracy: 0.9902
Epoch 6/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5527 - val_sparse_categorical_accuracy: 0.9902
Epoch 7/100
36/36 - 24s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5516 - val_sparse_categorical_accuracy: 0.9911
Epoch 8/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5513 - val_sparse_categorical_accuracy: 0.9920
Epoch 9/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5510 - val_sparse_categorical_accuracy: 0.9920
Epoch 10/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5505 - val_sparse_categorical_accuracy: 0.9920
Epoch 11/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5500 - val_sparse_categorical_accuracy: 0.9929
Epoch 12/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5499 - val_sparse_categorical_accuracy: 0.9929
Epoch 13/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5498 - val_sparse_categorical_accuracy: 0.9929
Epoch 14/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5496 - val_sparse_categorical_accuracy: 0.9929
Epoch 15/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5495 - val_sparse_categorical_accuracy: 0.9929
Epoch 16/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5495 - val_sparse_categorical_accuracy: 0.9929
Epoch 17/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5493 - val_sparse_categorical_accuracy: 0.9929
Epoch 18/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5493 - val_sparse_categorical_accuracy: 0.9929
Epoch 19/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5493 - val_sparse_categorical_accuracy: 0.9929
Epoch 20/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5493 - val_sparse_categorical_accuracy: 0.9938
Epoch 21/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5492 - val_sparse_categorical_accuracy: 0.9929
Epoch 22/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5492 - val_sparse_categorical_accuracy: 0.9938
Epoch 23/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5491 - val_sparse_categorical_accuracy: 0.9938
Epoch 24/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5491 - val_sparse_categorical_accuracy: 0.9938
Epoch 25/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5489 - val_sparse_categorical_accuracy: 0.9947
ResNet_Test14 loss 1.7021363973617554 acc 0.8285714387893677
sucess
