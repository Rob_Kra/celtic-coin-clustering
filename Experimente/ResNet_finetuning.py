# Transfer learning a CNN with the augmented  celtic coin Data.
# 11 Classes are chosen with 20+ origin pictures
import tensorflow as tf
import numpy as np
import os
import csv

print("Num GPUs Available: ", len(
    tf.config.experimental.list_physical_devices('GPU')))
# directory = "/home/robin/Pictures/celtic_tf_data/train/"
directory = "train/"
NN_name = "ResNet_" + os.getcwd().split('/')[-1]
img_size = 224  # Size of the imagenet pictures.
num_classes = 11
shape = (img_size, img_size, 3)
EPOCHS = 100

with open('performance.csv') as performance:
    csv_reader = csv.reader(performance, delimiter=',')
    line_count = 0
    best_acc = 0
    for row in csv_reader:
        if best_acc < float(row[4]):
            best_acc = float(row[4])
print('Best acc: ', best_acc)
# Creating datasets from a directory using tensorflows image preprocessing tool. Link below
# https://www.tensorflow.org/api_docs/python/tf/keras/preprocessing/image_dataset_from_directory
train = tf.keras.preprocessing.image_dataset_from_directory(
    directory, labels='inferred', label_mode='int', class_names=None,
    color_mode='rgb', batch_size=128, image_size=(img_size, img_size), shuffle=True, seed=123,
    validation_split=0.2, subset="training", interpolation='bilinear', follow_links=False
)
val = tf.keras.preprocessing.image_dataset_from_directory(
    directory, labels='inferred', label_mode='int', class_names=None,
    color_mode='rgb', batch_size=128, image_size=(img_size, img_size), shuffle=True, seed=123,
    validation_split=0.2, subset="validation", interpolation='bilinear', follow_links=False
)
test = tf.keras.preprocessing.image_dataset_from_directory(
    "test/",  labels='inferred', label_mode='int', class_names=None,
    color_mode='rgb', batch_size=1, image_size=(img_size, img_size),
    interpolation='bilinear', follow_links=False
)
# Normalize the data . Using Keras image preprocessing tool.


def prepare(ds):
    ds = ds.map(lambda x, y: (
        tf.keras.applications.resnet_v2.preprocess_input(x), y))
    return ds


train = prepare(train)
val = prepare(val)
test = prepare(test)
# load pretrained Model
# don`t include top layer. Pretrained imagenet weights.
cnv_base = tf.keras.applications.ResNet50V2(include_top=False,
                                            weights='imagenet', input_shape=shape, pooling='avg'
                                            )
CoinNet = tf.keras.Sequential()
CoinNet.add(cnv_base)
CoinNet.add(tf.keras.layers.Dense(num_classes, activation='softmax'))

for layer in cnv_base.layers:
    if not(layer.name.startswith('conv5') or layer.name.startswith('post')):
        layer.trainable = False
CoinNet.summary()


net_checkpoint = tf.keras.callbacks.ModelCheckpoint(
    filepath='best_weights.hdf5',
    monitor='val_sparse_categorical_accuracy',
    mode='auto',
    save_freq='epoch',
    save_best_only=True
)
net_early = tf.keras.callbacks.EarlyStopping(
    monitor='val_loss', min_delta=0.001, patience=10, verbose=0, mode='auto',
    baseline=None, restore_best_weights=False
)


net_callbacks = [
    net_checkpoint,
    net_early
]

CoinNet.compile(optimizer='adam',
                loss=tf.keras.losses.SparseCategoricalCrossentropy(
                    from_logits=True),
                metrics=[tf.keras.metrics.SparseCategoricalAccuracy()])

CoinNet.fit(train, validation_data=val, epochs=5, verbose=2)

test_loss, test_acc = CoinNet.evaluate(test, verbose=2)
print(NN_name, 'loss', test_loss, 'acc', test_acc)

save_name = NN_name + "_5epochs.h5"
# if best_acc  < test_acc:
#     print("saving", test_acc)
#     CoinNet.save(save_name)

csvRow = [save_name, 'loss', test_loss, 'acc', test_acc]
csvfile = "performance.csv"
with open(csvfile, "a") as fp:
    wr = csv.writer(fp, dialect='excel')
    wr.writerow(csvRow)


# changing learning rate
opt = tf.keras.optimizers.Adam(
    learning_rate=0.00001, beta_1=0.9, beta_2=0.999, epsilon=1e-07, amsgrad=False,
    name='Adam'
)

CoinNet.compile(optimizer=opt,
                loss=tf.keras.losses.SparseCategoricalCrossentropy(
                    from_logits=True),
                metrics=[tf.keras.metrics.SparseCategoricalAccuracy()])

CoinNet.fit(train, validation_data=val, epochs=EPOCHS,
            callbacks=net_callbacks, verbose=2)


test_loss, test_acc = CoinNet.evaluate(test, verbose=0)
print(NN_name, 'loss', test_loss, 'acc', test_acc)

save_name = NN_name + ".h5"

csvRow = [save_name, 'loss', test_loss, 'acc', test_acc]
csvfile = "performance.csv"
with open(csvfile, "a") as fp:
    wr = csv.writer(fp, dialect='excel')
    wr.writerow(csvRow)


if best_acc < test_acc:
    print("saving", test_acc)
    CoinNet.save(save_name)

print("sucess")
