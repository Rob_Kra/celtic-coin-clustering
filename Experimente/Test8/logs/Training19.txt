Num GPUs Available:  1
Best acc:  0.9714285731315613
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7267 - sparse_categorical_accuracy: 0.8341 - val_loss: 1.9498 - val_sparse_categorical_accuracy: 0.5904
Epoch 2/5
36/36 - 24s - loss: 1.5696 - sparse_categorical_accuracy: 0.9770 - val_loss: 1.6850 - val_sparse_categorical_accuracy: 0.8590
Epoch 3/5
36/36 - 23s - loss: 1.5562 - sparse_categorical_accuracy: 0.9880 - val_loss: 1.6107 - val_sparse_categorical_accuracy: 0.9326
Epoch 4/5
36/36 - 24s - loss: 1.5505 - sparse_categorical_accuracy: 0.9942 - val_loss: 1.5845 - val_sparse_categorical_accuracy: 0.9583
Epoch 5/5
36/36 - 24s - loss: 1.5598 - sparse_categorical_accuracy: 0.9867 - val_loss: 1.6737 - val_sparse_categorical_accuracy: 0.8750
35/35 - 0s - loss: 1.8268 - sparse_categorical_accuracy: 0.7143
ResNet_Test11 loss 1.8267625570297241 acc 0.7142857313156128
Epoch 1/100
36/36 - 24s - loss: 1.5486 - sparse_categorical_accuracy: 0.9951 - val_loss: 1.5779 - val_sparse_categorical_accuracy: 0.9663
Epoch 2/100
36/36 - 24s - loss: 1.5450 - sparse_categorical_accuracy: 0.9982 - val_loss: 1.5559 - val_sparse_categorical_accuracy: 0.9876
Epoch 3/100
36/36 - 24s - loss: 1.5435 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5488 - val_sparse_categorical_accuracy: 0.9947
Epoch 4/100
36/36 - 24s - loss: 1.5437 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5475 - val_sparse_categorical_accuracy: 0.9956
Epoch 5/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5473 - val_sparse_categorical_accuracy: 0.9956
Epoch 6/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5472 - val_sparse_categorical_accuracy: 0.9956
Epoch 7/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5472 - val_sparse_categorical_accuracy: 0.9947
Epoch 8/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5470 - val_sparse_categorical_accuracy: 0.9956
Epoch 9/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5467 - val_sparse_categorical_accuracy: 0.9956
Epoch 10/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5465 - val_sparse_categorical_accuracy: 0.9965
Epoch 11/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5464 - val_sparse_categorical_accuracy: 0.9965
Epoch 12/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5463 - val_sparse_categorical_accuracy: 0.9965
Epoch 13/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5462 - val_sparse_categorical_accuracy: 0.9965
Epoch 14/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5461 - val_sparse_categorical_accuracy: 0.9965
Epoch 15/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5461 - val_sparse_categorical_accuracy: 0.9965
Epoch 16/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5461 - val_sparse_categorical_accuracy: 0.9965
Epoch 17/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9973
Epoch 18/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5457 - val_sparse_categorical_accuracy: 0.9973
Epoch 19/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9973
Epoch 20/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9973
Epoch 21/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9973
ResNet_Test11 loss 1.6353468894958496 acc 0.9142857193946838
sucess
