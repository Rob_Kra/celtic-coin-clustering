Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7179 - sparse_categorical_accuracy: 0.8454 - val_loss: 1.8711 - val_sparse_categorical_accuracy: 0.6684
Epoch 2/5
36/36 - 23s - loss: 1.5589 - sparse_categorical_accuracy: 0.9878 - val_loss: 1.7817 - val_sparse_categorical_accuracy: 0.7624
Epoch 3/5
36/36 - 24s - loss: 1.5569 - sparse_categorical_accuracy: 0.9887 - val_loss: 1.5982 - val_sparse_categorical_accuracy: 0.9468
Epoch 4/5
36/36 - 23s - loss: 1.5528 - sparse_categorical_accuracy: 0.9911 - val_loss: 1.6400 - val_sparse_categorical_accuracy: 0.9034
Epoch 5/5
36/36 - 24s - loss: 1.5557 - sparse_categorical_accuracy: 0.9894 - val_loss: 1.6632 - val_sparse_categorical_accuracy: 0.8768
35/35 - 0s - loss: 1.8560 - sparse_categorical_accuracy: 0.6857
ResNet_Test11 loss 1.8560357093811035 acc 0.6857143044471741
Epoch 1/100
36/36 - 24s - loss: 1.5523 - sparse_categorical_accuracy: 0.9922 - val_loss: 1.5857 - val_sparse_categorical_accuracy: 0.9566
Epoch 2/100
36/36 - 24s - loss: 1.5472 - sparse_categorical_accuracy: 0.9973 - val_loss: 1.5578 - val_sparse_categorical_accuracy: 0.9858
Epoch 3/100
36/36 - 24s - loss: 1.5456 - sparse_categorical_accuracy: 0.9980 - val_loss: 1.5519 - val_sparse_categorical_accuracy: 0.9902
Epoch 4/100
36/36 - 24s - loss: 1.5449 - sparse_categorical_accuracy: 0.9987 - val_loss: 1.5489 - val_sparse_categorical_accuracy: 0.9956
Epoch 5/100
36/36 - 24s - loss: 1.5443 - sparse_categorical_accuracy: 0.9991 - val_loss: 1.5474 - val_sparse_categorical_accuracy: 0.9965
Epoch 6/100
36/36 - 24s - loss: 1.5439 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5463 - val_sparse_categorical_accuracy: 0.9973
Epoch 7/100
36/36 - 23s - loss: 1.5437 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5457 - val_sparse_categorical_accuracy: 0.9973
Epoch 8/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5452 - val_sparse_categorical_accuracy: 0.9982
Epoch 9/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9991
Epoch 10/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9991
Epoch 11/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 12/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 13/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9991
Epoch 14/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 15/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 16/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5440 - val_sparse_categorical_accuracy: 0.9991
Epoch 17/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5440 - val_sparse_categorical_accuracy: 0.9991
Epoch 18/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5440 - val_sparse_categorical_accuracy: 0.9991
Epoch 19/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5439 - val_sparse_categorical_accuracy: 1.0000
Epoch 20/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5439 - val_sparse_categorical_accuracy: 1.0000
Epoch 21/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5438 - val_sparse_categorical_accuracy: 1.0000
Epoch 22/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5438 - val_sparse_categorical_accuracy: 1.0000
Epoch 23/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5438 - val_sparse_categorical_accuracy: 1.0000
Epoch 24/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5438 - val_sparse_categorical_accuracy: 1.0000
Epoch 25/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5437 - val_sparse_categorical_accuracy: 1.0000
Epoch 26/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5437 - val_sparse_categorical_accuracy: 1.0000
ResNet_Test11 loss 1.6094510555267334 acc 0.9714285731315613
saving 0.9714285731315613
sucess
