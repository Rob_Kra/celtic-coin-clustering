Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7205 - sparse_categorical_accuracy: 0.8372 - val_loss: 1.7847 - val_sparse_categorical_accuracy: 0.7598
Epoch 2/5
36/36 - 23s - loss: 1.5628 - sparse_categorical_accuracy: 0.9825 - val_loss: 1.6559 - val_sparse_categorical_accuracy: 0.8883
Epoch 3/5
36/36 - 23s - loss: 1.5577 - sparse_categorical_accuracy: 0.9869 - val_loss: 1.6414 - val_sparse_categorical_accuracy: 0.9051
Epoch 4/5
36/36 - 24s - loss: 1.5528 - sparse_categorical_accuracy: 0.9907 - val_loss: 1.6222 - val_sparse_categorical_accuracy: 0.9211
Epoch 5/5
36/36 - 24s - loss: 1.5516 - sparse_categorical_accuracy: 0.9931 - val_loss: 1.5632 - val_sparse_categorical_accuracy: 0.9787
35/35 - 0s - loss: 1.6582 - sparse_categorical_accuracy: 0.8857
ResNet_Test11 loss 1.6581777334213257 acc 0.8857142925262451
Epoch 1/100
36/36 - 24s - loss: 1.5465 - sparse_categorical_accuracy: 0.9967 - val_loss: 1.5535 - val_sparse_categorical_accuracy: 0.9902
Epoch 2/100
36/36 - 24s - loss: 1.5443 - sparse_categorical_accuracy: 0.9991 - val_loss: 1.5501 - val_sparse_categorical_accuracy: 0.9929
Epoch 3/100
36/36 - 24s - loss: 1.5436 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5479 - val_sparse_categorical_accuracy: 0.9965
Epoch 4/100
36/36 - 23s - loss: 1.5437 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5468 - val_sparse_categorical_accuracy: 0.9965
Epoch 5/100
36/36 - 24s - loss: 1.5437 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5462 - val_sparse_categorical_accuracy: 0.9973
Epoch 6/100
36/36 - 24s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9973
Epoch 7/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5453 - val_sparse_categorical_accuracy: 0.9982
Epoch 8/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9982
Epoch 9/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9991
Epoch 10/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9991
Epoch 11/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9991
Epoch 12/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9991
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9991
Epoch 14/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9991
Epoch 15/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9991
Epoch 16/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9991
Epoch 17/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9991
ResNet_Test11 loss 1.628160834312439 acc 0.9142857193946838
sucess
