Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7219 - sparse_categorical_accuracy: 0.8364 - val_loss: 1.7182 - val_sparse_categorical_accuracy: 0.8236
Epoch 2/5
36/36 - 24s - loss: 1.5646 - sparse_categorical_accuracy: 0.9807 - val_loss: 1.5865 - val_sparse_categorical_accuracy: 0.9592
Epoch 3/5
36/36 - 24s - loss: 1.5611 - sparse_categorical_accuracy: 0.9843 - val_loss: 1.5864 - val_sparse_categorical_accuracy: 0.9592
Epoch 4/5
36/36 - 24s - loss: 1.5500 - sparse_categorical_accuracy: 0.9938 - val_loss: 1.6172 - val_sparse_categorical_accuracy: 0.9273
Epoch 5/5
36/36 - 24s - loss: 1.5637 - sparse_categorical_accuracy: 0.9805 - val_loss: 1.6488 - val_sparse_categorical_accuracy: 0.8945
35/35 - 0s - loss: 1.8547 - sparse_categorical_accuracy: 0.6857
ResNet_Test13 loss 1.8546950817108154 acc 0.6857143044471741
Epoch 1/100
36/36 - 24s - loss: 1.5486 - sparse_categorical_accuracy: 0.9947 - val_loss: 1.5725 - val_sparse_categorical_accuracy: 0.9734
Epoch 2/100
36/36 - 24s - loss: 1.5456 - sparse_categorical_accuracy: 0.9978 - val_loss: 1.5522 - val_sparse_categorical_accuracy: 0.9920
Epoch 3/100
36/36 - 24s - loss: 1.5443 - sparse_categorical_accuracy: 0.9991 - val_loss: 1.5477 - val_sparse_categorical_accuracy: 0.9956
Epoch 4/100
36/36 - 24s - loss: 1.5439 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5463 - val_sparse_categorical_accuracy: 0.9965
Epoch 5/100
36/36 - 23s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5453 - val_sparse_categorical_accuracy: 0.9982
Epoch 6/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5450 - val_sparse_categorical_accuracy: 0.9982
Epoch 7/100
36/36 - 24s - loss: 1.5435 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9991
Epoch 8/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9991
Epoch 9/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9991
Epoch 10/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9991
Epoch 11/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9991
Epoch 12/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9991
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9991
Epoch 14/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9991
Epoch 15/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
ResNet_Test13 loss 1.6692087650299072 acc 0.8571428656578064
sucess
