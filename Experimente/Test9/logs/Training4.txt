Num GPUs Available:  1
Best acc:  0.9142857193946838
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7098 - sparse_categorical_accuracy: 0.8492 - val_loss: 1.8534 - val_sparse_categorical_accuracy: 0.6844
Epoch 2/5
36/36 - 23s - loss: 1.5609 - sparse_categorical_accuracy: 0.9854 - val_loss: 1.7269 - val_sparse_categorical_accuracy: 0.8156
Epoch 3/5
36/36 - 23s - loss: 1.5578 - sparse_categorical_accuracy: 0.9874 - val_loss: 1.6436 - val_sparse_categorical_accuracy: 0.9043
Epoch 4/5
36/36 - 24s - loss: 1.5501 - sparse_categorical_accuracy: 0.9938 - val_loss: 1.7116 - val_sparse_categorical_accuracy: 0.8342
Epoch 5/5
36/36 - 24s - loss: 1.5553 - sparse_categorical_accuracy: 0.9900 - val_loss: 1.6963 - val_sparse_categorical_accuracy: 0.8484
35/35 - 0s - loss: 1.8756 - sparse_categorical_accuracy: 0.6857
ResNet_Test13 loss 1.8755700588226318 acc 0.6857143044471741
Epoch 1/100
36/36 - 24s - loss: 1.5498 - sparse_categorical_accuracy: 0.9940 - val_loss: 1.5850 - val_sparse_categorical_accuracy: 0.9574
Epoch 2/100
36/36 - 24s - loss: 1.5462 - sparse_categorical_accuracy: 0.9976 - val_loss: 1.5563 - val_sparse_categorical_accuracy: 0.9858
Epoch 3/100
36/36 - 24s - loss: 1.5442 - sparse_categorical_accuracy: 0.9993 - val_loss: 1.5482 - val_sparse_categorical_accuracy: 0.9947
Epoch 4/100
36/36 - 24s - loss: 1.5437 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5464 - val_sparse_categorical_accuracy: 0.9965
Epoch 5/100
36/36 - 25s - loss: 1.5435 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5458 - val_sparse_categorical_accuracy: 0.9973
Epoch 6/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5454 - val_sparse_categorical_accuracy: 0.9973
Epoch 7/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5451 - val_sparse_categorical_accuracy: 0.9991
Epoch 8/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5449 - val_sparse_categorical_accuracy: 0.9991
Epoch 9/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5448 - val_sparse_categorical_accuracy: 0.9991
Epoch 10/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5447 - val_sparse_categorical_accuracy: 0.9991
Epoch 11/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5446 - val_sparse_categorical_accuracy: 0.9991
Epoch 12/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 14/100
36/36 - 25s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 15/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 16/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 17/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 18/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 19/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5444 - val_sparse_categorical_accuracy: 0.9991
Epoch 20/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5443 - val_sparse_categorical_accuracy: 0.9991
Epoch 21/100
36/36 - 24s - loss: 1.5432 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5442 - val_sparse_categorical_accuracy: 1.0000
Epoch 22/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5442 - val_sparse_categorical_accuracy: 1.0000
Epoch 23/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5442 - val_sparse_categorical_accuracy: 1.0000
Epoch 24/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 1.0000
Epoch 25/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 1.0000
Epoch 26/100
36/36 - 24s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 1.0000
ResNet_Test13 loss 1.7157094478607178 acc 0.8285714387893677
sucess
