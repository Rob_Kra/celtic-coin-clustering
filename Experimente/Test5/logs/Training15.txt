Num GPUs Available:  1
Best acc:  0.9428571462631226
Found 5644 files belonging to 11 classes.
Using 4516 files for training.
Found 5644 files belonging to 11 classes.
Using 1128 files for validation.
Found 35 files belonging to 11 classes.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
resnet50v2 (Functional)      (None, 2048)              23564800  
_________________________________________________________________
dense (Dense)                (None, 11)                22539     
=================================================================
Total params: 23,587,339
Trainable params: 14,993,419
Non-trainable params: 8,593,920
_________________________________________________________________
Epoch 1/5
36/36 - 25s - loss: 1.7234 - sparse_categorical_accuracy: 0.8372 - val_loss: 1.8553 - val_sparse_categorical_accuracy: 0.6888
Epoch 2/5
36/36 - 23s - loss: 1.5645 - sparse_categorical_accuracy: 0.9812 - val_loss: 1.6408 - val_sparse_categorical_accuracy: 0.9025
Epoch 3/5
36/36 - 23s - loss: 1.5635 - sparse_categorical_accuracy: 0.9821 - val_loss: 1.6467 - val_sparse_categorical_accuracy: 0.8936
Epoch 4/5
36/36 - 23s - loss: 1.5543 - sparse_categorical_accuracy: 0.9907 - val_loss: 1.6997 - val_sparse_categorical_accuracy: 0.8457
Epoch 5/5
36/36 - 23s - loss: 1.5492 - sparse_categorical_accuracy: 0.9945 - val_loss: 1.5552 - val_sparse_categorical_accuracy: 0.9876
35/35 - 0s - loss: 1.6786 - sparse_categorical_accuracy: 0.8571
ResNet_Test7 loss 1.6785907745361328 acc 0.8571428656578064
Epoch 1/100
36/36 - 24s - loss: 1.5458 - sparse_categorical_accuracy: 0.9978 - val_loss: 1.5479 - val_sparse_categorical_accuracy: 0.9956
Epoch 2/100
36/36 - 23s - loss: 1.5439 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5456 - val_sparse_categorical_accuracy: 0.9973
Epoch 3/100
36/36 - 23s - loss: 1.5435 - sparse_categorical_accuracy: 0.9996 - val_loss: 1.5445 - val_sparse_categorical_accuracy: 0.9991
Epoch 4/100
36/36 - 23s - loss: 1.5434 - sparse_categorical_accuracy: 0.9998 - val_loss: 1.5441 - val_sparse_categorical_accuracy: 0.9991
Epoch 5/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5439 - val_sparse_categorical_accuracy: 0.9991
Epoch 6/100
36/36 - 24s - loss: 1.5433 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5438 - val_sparse_categorical_accuracy: 1.0000
Epoch 7/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5437 - val_sparse_categorical_accuracy: 1.0000
Epoch 8/100
36/36 - 23s - loss: 1.5432 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5437 - val_sparse_categorical_accuracy: 1.0000
Epoch 9/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5437 - val_sparse_categorical_accuracy: 1.0000
Epoch 10/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5436 - val_sparse_categorical_accuracy: 1.0000
Epoch 11/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5436 - val_sparse_categorical_accuracy: 1.0000
Epoch 12/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5435 - val_sparse_categorical_accuracy: 1.0000
Epoch 13/100
36/36 - 23s - loss: 1.5431 - sparse_categorical_accuracy: 1.0000 - val_loss: 1.5436 - val_sparse_categorical_accuracy: 1.0000
ResNet_Test7 loss 1.6399774551391602 acc 0.9142857193946838
sucess
