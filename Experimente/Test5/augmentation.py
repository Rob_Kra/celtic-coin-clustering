# This script is for data augmentation.
# It uses the ImageDataGenerator class from Keras/Tensorflow
# https://www.tensorflow.org/api_docs/python/tf/keras/preprocessing/image/ImageDataGenerator
import os
import tensorflow as tf
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from math import ceil
from tqdm import tqdm


class augmentation():

    def __init__(self, data_path, aug_arg, number_pic=500):
        self.data_path = data_path
        self.data_gen_args = aug_arg
        self.data_generator = ImageDataGenerator(**self.data_gen_args)
        self.number_pic = number_pic
        self.classes = os.listdir(data_path)

    def genearte_picture(self):
        for folder in tqdm(self.classes):
            folder_path = os.path.join(self.data_path, folder)
            existing_img = len(os.listdir(folder_path))
            # max number of augmentation per image
            num_aug = ceil(self.number_pic / existing_img)
            print('Anzahl an Augmentationen', num_aug-1, 'Ordner', folder_path)
            seed = self.classes.index(folder)
            for image in os.listdir(folder_path):
                x = load_img(os.path.join(folder_path, image))
                x = img_to_array(x)
                x = x.reshape((1,) + x.shape)
                k = 0  # number of augementations performed per image
                for batch in self.data_generator.flow(x, batch_size=1, save_to_dir=folder_path,
                                                      save_prefix=image.split('.')[
                                                          0],
                                                      save_format='jpeg', seed=seed):
                    k += 1
                    if k >= num_aug-1:
                        break


def __main__():
    data_gen_args = dict(
        # Degree range for random rotations. Between 0-180
        rotation_range=45,
        # Range for a width shift of the picture.
        width_shift_range=0.1,
        # Range for a heigth shift of the picture.
        height_shift_range=0.1,
        # Range for  random zoom. Range : [1-zoom,1+zoom]
        zoom_range=0,
        # Randomly flip inputs horizontally if True
        horizontal_flip=True,
        # Specify how the points outside the  boundaries of the input should be filled.
        # Best results with constant(Black edges)
        fill_mode='nearest',
        # Random brightnes's shift in range of the two values
        brightness_range=None
    )
    aug = augmentation('train', data_gen_args)
    aug.genearte_picture()


if __name__ == '__main__':
    __main__()
