"""
This Code is from https://github.com/keras-team/keras-io/blob/master/examples/vision/grad_cam.py
It was changed  a little  to match the CNN used in the thesis. Changed 1/10/2020
Originial Author:
    Title: Grad-CAM class activation visualization
    Author: [fchollet](https://twitter.com/fchollet)
    Date created: 2020/04/26
    Last modified: 2020/05/14
    Description: How to obtain a class activation heatmap for an image classification model.
    Adapted from Deep Learning with Python (2017).
"""


from tqdm import tqdm
import os
from tqdm import tqdm
import cv2
from tensorflow import keras
from IPython.display import Image
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import tensorflow as tf
import numpy as np
img_size = (224, 224)


def make_gradcam_heatmap(
    img_array, model, last_conv_layer_name, classifier_layer_names
):

    # First, we create a model that maps the input image to the activations
    # of the last conv layer
    # !!! CHANGES !!! layers[0] insted of layers to match the structure.
    last_conv_layer = model.layers[0].get_layer(last_conv_layer_name)
    last_conv_layer_model = keras.Model(
        model.layers[0].inputs, last_conv_layer.output)

    # Second, we create a model that maps the activations of the last conv
    # layer to the final class predictions
    classifier_input = keras.Input(shape=last_conv_layer.output.shape[1:])
    x = classifier_input
    # !!! CHANGES !!!
    workaround = 0
    for layer_name in classifier_layer_names:
        if workaround == 0:
            x = model.layers[0].get_layer(layer_name)(x)
            workaround += 1
        else:
            x = model.get_layer(layer_name)(x)
    classifier_model = keras.Model(classifier_input, x)

    # Then, we compute the gradient of the top predicted class for our input image
    # with respect to the activations of the last conv layer
    with tf.GradientTape() as tape:
        # Compute activations of the last conv layer and make the tape watch it
        last_conv_layer_output = last_conv_layer_model(img_array)
        tape.watch(last_conv_layer_output)
        # Compute class predictions
        preds = classifier_model(last_conv_layer_output)
        top_pred_index = tf.argmax(preds[0])
        top_class_channel = preds[:, top_pred_index]

    # This is the gradient of the top predicted class with regard to
    # the output feature map of the last conv layer
    grads = tape.gradient(top_class_channel, last_conv_layer_output)

    # This is a vector where each entry is the mean intensity of the gradient
    # over a specific feature map channel
    pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))

    # We multiply each channel in the feature map array
    # by "how important this channel is" with regard to the top predicted class
    last_conv_layer_output = last_conv_layer_output.numpy()[0]
    pooled_grads = pooled_grads.numpy()
    for i in range(pooled_grads.shape[-1]):
        last_conv_layer_output[:, :, i] *= pooled_grads[i]

    # The channel-wise mean of the resulting feature map
    # is our heatmap of class activation
    heatmap = np.mean(last_conv_layer_output, axis=-1)

    # For visualization purpose, we will also normalize the heatmap between 0 & 1
    heatmap = np.maximum(heatmap, 0) / np.max(heatmap)
    return heatmap


"""
Functions to load images.
"""


def load_image(path, size=img_size):
    img_array = cv2.imread(path)
    img_array = cv2.resize(img_array, size)
    img_array = tf.keras.applications.resnet_v2.preprocess_input(
        img_array).reshape(-1, 224, 224, 3)
    return img_array


def load_image_without_preproc(path, size=img_size):
    img_array = cv2.imread(path)
    img_array = cv2.resize(img_array, size)
    return img_array


"""
for displaying the labels of the Coins
"""


def find_label(img_name, ground_path):
    for folder in os.listdir(ground_path):
        folder_path = os.path.join(ground_path, folder)
        test_path = os.path.join(folder_path, img_name)
        if folder == 'NL':
            folder = 'Unlabeled '
        if os.path.isfile(test_path):
            return folder


"""
This function displays a heatmap. Code of the example.
"""


def display_heatmap(model, model_name, last_conv_layer_name,
                    classifier_layer_names, img_path, categories_path, save_path='./'):
    # # Loading Image and predicting output
    # Categories = os.listdir(categories_path)
    # Categories.sort()
    img_array = load_image(img_path)
    # pred = model.predict(img_array)
    # print("Predicted:", Categories[pred.argmax()])

    # generate heatmap
    heatmap = make_gradcam_heatmap(
        img_array, model, last_conv_layer_name, classifier_layer_names
    )
    # processing heatmap and saving/displaying image
    img = keras.preprocessing.image.load_img(img_path)
    img = keras.preprocessing.image.img_to_array(img)
    # We rescale heatmap to a range 0-255
    heatmap = np.uint8(255 * heatmap)
    # We use jet colormap to colorize heatmap
    jet = cm.get_cmap("jet")
    # We use RGB values of the colormap
    jet_colors = jet(np.arange(256))[:, :3]
    jet_heatmap = jet_colors[heatmap]
    # We create an image with RGB colorized heatmap
    jet_heatmap = keras.preprocessing.image.array_to_img(jet_heatmap)
    jet_heatmap = jet_heatmap.resize((img.shape[1], img.shape[0]))
    jet_heatmap = keras.preprocessing.image.img_to_array(jet_heatmap)
    # Superimpose the heatmap on original image
    superimposed_img = jet_heatmap * 0.4 + img
    # superimpose_array = superimposed_img
    superimposed_img = keras.preprocessing.image.array_to_img(superimposed_img)

    ground_truth = find_label(img_path.split('/')[-1], categories_path)

    # Save the superimposed image
    save_name = 'GradCam_' + model_name + '_GT_' + \
        ground_truth + '_' + img_path.split('/')[-1]
    save_path = os.path.join(save_path, save_name)
    superimposed_img.save(save_path)


def generate_heatmaps(folder_path, model, model_name, last_conv_layer_name,
                      classifier_layer_names, categories_path, save_path='./'):
    """
    geneate a heatmap of one picture
    """
    for img in tqdm(os.listdir(folder_path)):
        img_path = os.path.join(folder_path, img)
        display_heatmap(model, model_name, last_conv_layer_name,
                        classifier_layer_names, img_path, categories_path, save_path)


def plot_all_coins(folder_path, ground_path, pics_per_row=4, save_folder='', save_format='png', dpi=500):
    """
    This function is for displaying all Pictures of one Cluster.
    Name and ground truth are also displayed.
    Dpi changes the size of the picture. If u change dpi also change the fontsize of the title strings.

    """
    # plt.rcParams.update({'axes.titlesize': 'small'})

    num_pic = len(os.listdir(folder_path))
    num_cols = pics_per_row
    num_rows = num_pic // num_cols + 1
    fig, ax = plt.subplots(ncols=num_cols, nrows=num_rows, figsize=(
        5*num_cols, 5*num_rows), constrained_layout=True)

    images = os.listdir(folder_path)
    images.sort()
    for x, img in enumerate(images):
        col = x % num_cols
        row = x // num_cols
        image = plt.imread(os.path.join(folder_path, img))
        label = find_label(img.split('_')[-1], ground_path)
        ax[row][col].imshow(image)
        image_title = img.split(
            '.')[0].split('_')[-1] + ' ,' + label
        ax[row][col].set_title(
            image_title, fontsize=20)
        ax[row][col].axis('off')

    title_string = 'All pictures from cluster ' + \
        folder_path.split('/')[-1] + '\n'
    # CHANGE THE SIZE IF YOU CHANGE DPI
    fig.suptitle(title_string, fontsize=30)
    save_name = 'cluster_' + \
        folder_path.split('/')[-1] + '_overview.' + save_format
    save_folder = save_folder
    save_name = os.path.join(save_folder, save_name)
    # dpis = num_pic * 1000
    plt.savefig(fname=save_name, dpi=dpi)   # , dpi=2000)
    # plt.show()
    plt.close(fig)


def return_heatmap_and_label(model, model_name, last_conv_layer_name,
                             classifier_layer_names, img_path, categories_path, save_path='./'):

    img_array = load_image(img_path)
    # generate heatmap
    heatmap = make_gradcam_heatmap(
        img_array, model, last_conv_layer_name, classifier_layer_names
    )
    # processing heatmap and saving/displaying image
    img = keras.preprocessing.image.load_img(img_path)
    img = keras.preprocessing.image.img_to_array(img)
    # We rescale heatmap to a range 0-255
    heatmap = np.uint8(255 * heatmap)
    # We use jet colormap to colorize heatmap
    jet = cm.get_cmap("jet")
    # We use RGB values of the colormap
    jet_colors = jet(np.arange(256))[:, :3]
    jet_heatmap = jet_colors[heatmap]
    # We create an image with RGB colorized heatmap
    jet_heatmap = keras.preprocessing.image.array_to_img(jet_heatmap)
    jet_heatmap = jet_heatmap.resize((img.shape[1], img.shape[0]))
    jet_heatmap = keras.preprocessing.image.img_to_array(jet_heatmap)
    # Superimpose the heatmap on original image
    superimposed_img = jet_heatmap * 0.4 + img
    # superimpose_array = superimposed_img
    superimposed_img = keras.preprocessing.image.array_to_img(superimposed_img)

    ground_truth = find_label(img_path.split('/')[-1], categories_path)

    return superimposed_img, ground_truth


def plot_all_heatmaps(folder_path, ground_path, Net,
                      layer_names=['post_relu', ['avg_pool', 'dense']], pics_per_row=4, save_folder='', save_format='png', dpi=500):
    """
    This function is for displaying all Pictures of one Cluster.
    Name and ground truth are also displayed.
    Dpi changes the size of the picture. If u change dpi also change the fontsize of the title strings.
    This will work for ResNet. For anthoer structure you have to change layer_names
    """
    num_pic = len(os.listdir(folder_path))
    num_cols = pics_per_row
    num_rows = num_pic // num_cols + 1
    fig, ax = plt.subplots(ncols=num_cols, nrows=num_rows, figsize=(
        5*num_cols, 5*num_rows), constrained_layout=True)

    images = os.listdir(folder_path)
    images.sort()
    for x, img in enumerate(images):
        col = x % num_cols
        row = x // num_cols
        img_path = os.path.join(folder_path, img)
        image, label = return_heatmap_and_label(
            Net, 'Net', layer_names[0], layer_names[1], img_path, ground_path)
        ax[row][col].imshow(image)
        image_title = img.split(
            '.')[0].split('_')[-1] + ' ,' + label
        ax[row][col].set_title(
            image_title, fontsize=20)  # CHANGE HERE
        ax[row][col].axis('off')

    title_string = 'All pictures from cluster ' + \
        folder_path.split('/')[-1] + '\n'
    fig.suptitle(title_string, fontsize=15)
    save_name = 'cluster_' + \
        folder_path.split('/')[-1] + '_heatmap_overview.' + save_format
    save_folder = save_folder
    save_name = os.path.join(save_folder, save_name)
    # dpis = num_pic * 1000
    plt.savefig(fname=save_name, dpi=dpi)   # , dpi=2000)
    # plt.show()
    plt.close(fig)


def main(x):
    Net = tf.keras.models.load_model('ResNet_Test14.h5')
    modelname = 'Net14'
    last_conv_layer_name = 'post_relu'
    classifier_layer_names = ['avg_pool', 'dense']
    categories = '/home/robin/Pictures/classes'
    folder_path = '/home/robin/Pictures/KMeans_Cluster/Kmeans/Cluster/' + x
    save_path = '/home/robin/Pictures/Heatmaps/' + x
    generate_heatmaps(folder_path, Net, modelname, last_conv_layer_name,
                      classifier_layer_names, categories, save_path)
