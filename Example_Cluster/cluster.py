from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
import cv2
import seaborn as sns
from matplotlib import pyplot as plt
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.manifold import TSNE
from sklearn.metrics import homogeneity_completeness_v_measure, adjusted_rand_score
from sklearn.cluster import KMeans, AgglomerativeClustering, DBSCAN, OPTICS
from tqdm import tqdm
import matplotlib.ticker as mticker
import csv
import numpy as np
import os
import tensorflow as tf
import pandas as pd

sns.set(rc={'figure.figsize': (10, 10)})
# plt.rcParams["figure.figsize"] = (400, 400)


class Clustering:

    def __init__(self, image_dir, cnn=None,  img_size=(224, 224), labeled=False, extract_features=False):
        """
        image_dir: path to the data. If labeled is set True, the data will be labeld by the structure of the folder.
        cnn: Convultional neural net for Feature Extraction. Only works with Tensorflow
        img_size: Image size for preprocessing. Must match the input size of the CNN.
        labeld: If True the data will be labeled given by the folder structure.
        extract_features: If True the extracted features for  the clustering will be generated,
        if False ex_features  equals raw image arrays. Does not work without an CNN
        """
        self.image_dir = image_dir
        self.img_size = img_size
        self.true_label = []
        self.raw_data = []
        self.Categories = os.listdir(image_dir)

        if labeled:
            self.true_label = []
            self.raw_data = []
            print("Loading data with label:")
            for folder in tqdm(os.listdir(image_dir)):
                path_to_file = os.path.join(image_dir, folder)
                class_num = self.Categories.index(folder)
                for picture in os.listdir(path_to_file):
                    file_name = os.path.join(path_to_file, picture)
                    img_array = cv2.imread(file_name)
                    img_array = cv2.resize(img_array, img_size)
                    # The data must be saved with the name. Otherwise the Cluster can´t be saved.
                    self.raw_data.append([file_name, img_array])
                    self.true_label.append(class_num)
        else:
            self.raw_data = []
            print("Loading only data:")
            for folder in tqdm(os.listdir(image_dir)):
                path_to_file = os.path.join(image_dir, folder)
                for picture in os.listdir(path_to_file):
                    file_name = os.path.join(path_to_file, picture)
                    img_array = cv2.imread(file_name)
                    img_array = cv2.resize(img_array, img_size)
                    self.raw_data.append([file_name, img_array])

        if extract_features & (cnn != None):
            self.load_Nets(cnn)
            self.extract_features()
        else:
            self.ex_features = []
            for (name, picture) in self.raw_data:
                self.ex_features.append(picture)

    def load_Nets(self, Net):
        """
            Loads the CNN and the Feature Extraction Net.
            Only works with Tensorflow. Can be changed for Pytorch etc.
        """
        self.Net = tf.keras.models.load_model(Net)
        self.feature_extraction_Net = tf.keras.Sequential()
        for layer in self.Net.layers[:-1]:
            self.feature_extraction_Net.add(layer)

    def extract_features(self, ResNet=True):
        """
            If ResNet = True for preprocessing the data.
            If different CNN architecture is used don't forget to properly preprocess the data!!
        """
        self.ex_features = []
        print("Extracting features:")
        for x in tqdm(self.raw_data):
            if ResNet:
                img = tf.keras.applications.resnet_v2.preprocess_input(
                    x[1]).reshape(-1, 224, 224, 3)
            else:
                img = x[1].reshape(-1, 224, 224, 3)
            img = self.feature_extraction_Net.predict(img)
            self.ex_features.append(img)
        self.ex_features = np.array(self.ex_features).reshape(-1, 2048)
        self.ex_features = self.ex_features
        self.tsne = TSNE(n_iter=3000).fit_transform(
            self.ex_features)  # For visualization

    def load_raw(self, path):
        """
        Loading raw_data from a file.
        """
        self.raw_data = np.load(path, allow_pickle=True)

    def print_metrics(self):
        print("Homogeneity: ", self.homogeneity)
        print("Compltness: ", self.completness)
        print("V Measure: ", self.v_measure)

    def eval_on_set(self, eval_labels=[]):
        cluster_labels = []
        true_labels = []
        index = 0
        for lab in self.labels:
            picture = self.raw_data[index][0]
            pic_lab = picture.split('/')[-2]
            if pic_lab in eval_labels:
                cluster_labels.append(lab)
                true_labels.append(self.true_label[index])
            index += 1
        homogeneity, completness, v_measure = homogeneity_completeness_v_measure(
            true_labels, cluster_labels)
        print('Scores on the Labels: ', eval_labels, '\n',
              'Homogenity Score: ', homogeneity, '\n',
              'Completness Score: ', completness, '\n',
              'v_measure: ', v_measure, '\n'
              )

    def create_KMeansClustering(self, **kwargs):
        """
            Creates a K-means Clustering.
            Look up the Kwargs at https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
        """

        self.KmeansCluster = KMeans(
            **kwargs).fit(self.ex_features)  # algorithm='elkan'

        self.labels = self.KmeansCluster.labels_

        self.inertia = self.KmeansCluster.inertia_

        self.homogeneity, self.completness, self.v_measure = homogeneity_completeness_v_measure(
            self.true_label, self.labels)
        self.Cluster_center = self.KmeansCluster.cluster_centers_
        self.create_overview()
        self.print_metrics()

    def find_closest_pictures(self, nums=10):
        """
        Function which finds the nearest points to the cluster centre
        """
        self.closest = []
        distances = euclidean_distances(self.Cluster_center, self.ex_features)

        for i in range(len(distances)):
            closest = []
            for x in range(nums):

                index = int(np.where(distances[i] == np.amin(distances[i]))[0])
                closest.append(index)
                distances[i][index] = float('Inf')
            self.closest.append(closest)

    def save_closest(self, destination):
        # savin closest points for new training
        if os.path.exists(destination):
            self.create_folder(destination, size=(len(self.closest)))
            folder_number = 0
            for centroid in tqdm(self.closest):
                for index in centroid:
                    picture = self.raw_data[index][0]
                    # For Windows this part must be changed
                    name = picture.split('/')[-1]
                    new_folder = os.path.join(
                        destination, str(folder_number), name)
                    # For Windows this part must be changed
                    command = 'cp '+picture+' '+new_folder
                    os.system(command)
                folder_number += 1

    def create_AgglomerativeClustering(self, n_cluster=1, method='ward'):
        """
        Creates a Agglomerative Clustering.
        n_cluster determines the cluster size. See fcluster
        """
        self.linkage = linkage(self.ex_features, method=method)
        self.labels = fcluster(self.linkage, criterion='maxclust', t=n_cluster)
        self.homogeneity, self.completness, self.v_measure = homogeneity_completeness_v_measure(
            self.true_label, self.labels)
        self.create_overview()
        self.print_metrics()

    def remove_cluster(self, destination):
        """
        Removes an existing Cluster at path = destination.
        Only tested on Linux.
        """
        if os.path.exists(destination):
            n_cluster = os.listdir(destination)
            for i in n_cluster:
                path = os.path.join(destination, str(i))
                command = 'rm -r ' + path  # For Windows this part must be changed
                os.system(command)
        else:
            print("Wrong path")

    def create_folder(self, destination, size=0):
        if os.path.exists(destination):
            for i in range(size):
                path = os.path.join(destination, str(i))
                os.mkdir(path)
        else:
            print("Wrong path")

    def plot_dendrogram(self, **kwargs):
        """
        plots the dendrogram of the hierarchical cluster.
        kwargs see https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.dendrogram.html
        """
        dendrogram(self.linkage, **kwargs)

    def plot_cluster_dendrogram(self):
        """
       plots the dendrogram of the hierarchical cluster with the cluster labels.
       kwargs see https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.dendrogram.html
       """
        dend = dendrogram(
            self.linkage,
            truncate_mode='lastp',  # show only the last p merged clusters
            # show only the last p merged clusters
            p=len(np.unique(self.labels)),
            no_plot=True,
        )

        cluster_labels = {dend["leaves"][x]: np.unique(
            self.labels)[x] for x in range(len(dend["leaves"]))}

        def label_func(z):
            return "{}".format(cluster_labels[z])
        dendrogram(
            self.linkage,
            truncate_mode='lastp',  # show only the last p merged clusters
            # show only the last p merged clusters
            p=len(np.unique(self.labels)),
            leaf_label_func=label_func,
            leaf_rotation=60.,
            leaf_font_size=12.,
            color_threshold=0,
            show_contracted=True,  # to get a distribution impression in truncated branches
        )

    def visual_data(self, true_label=True):
        """
            TSNE is used for Dimension Reduction.
            Visualization of the extracted features.

        """
        if true_label:
            labels = []
            for i in self.true_label:
                labels.append(self.Categories[i])
        else:
            labels = self.labels
        sns.scatterplot(self.tsne[:, 0], self.tsne[:, 1],
                        hue=labels, legend='full', palette=sns.color_palette("hls", len(np.unique(labels))))
        plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plt.show()

    def save_cluster(self, destination_cluster, destination_data='/tmp', save_data=False):
        """
            Saves the Cluster in folder hierarchy
            destination_cluster:  save path for cluster
            destination_data:     save path for ground_truth and raw_data
            save_data:            Bool. If True gorund_truh and raw_data will be saved.
        """
        if not(os.path.exists(destination_cluster)):
            os.mkdir(destination_cluster)
        self.create_folder(destination_cluster,
                           size=len(np.unique(self.labels)))
        index = 0
        for i in tqdm(self.labels):
            picture = self.raw_data[index][0]
            # For Windows this part may need to be changed
            name = picture.split('/')[-1]
            name = self.Categories[self.true_label[index]] + '_' + name
            new_folder = os.path.join(destination_cluster, str(i), name)
            # For Windows this part may need to be changed
            command = 'cp '+picture+' '+new_folder
            os.system(command)
            index += 1
        if save_data:
            if not(os.path.exists(destination_data)):
                os.mkdir(destination_data)
            y_path = os.path.join(destination_data, "ground_truth")
            X_path = os.path.join(destination_data, "raw_data")
            ex_path = os.path.join(destination_data, "extracted_features")
            np.save(y_path, self.true_label)
            np.save(X_path, self.raw_data)
            np.save(ex_path, self.ex_features)
            self.write_csv(os.path.join(destination_data, 'Info.csv'))

    def write_csv(self, save_name):
        """
            Writes an csv-file which contains follwing info about each image: name, cluster , ground truth
            save_name: save path. 

        """
        os.system("touch " + save_name)
        with open(save_name, 'w', newline='') as csvfile:
            field_names = ['name', 'stempel', 'ground truth']
            writer = csv.DictWriter(csvfile, fieldnames=field_names)
            writer.writeheader()
            index = 0
            for i in tqdm(self.labels):
                image = self.raw_data[index][0]
                # For Windows this part may need to be changed
                p_name = image.split('/')[-1]
                real_class = self.Categories[self.true_label[index]]
                if real_class == 'NL':
                    real_class = 'Unlabeled '
                writer.writerow(
                    {'name': p_name, 'stempel': str(i), 'ground truth': real_class})
                index += 1

    def create_overview(self):
        """
            This function will provide valuable info about the cluster
            overview shows of each image the Name, in which cluster it is, what ground truth it has. 
        """
        data = []
        field_names = ['Name', 'Cluster', 'Ground truth', 'Anzahl']
        index = 0
        print('Creating the overviews')
        for i in tqdm(self.labels):
            picture = self.raw_data[index][0]
            # For Windows this part may need to be changed
            p_name = picture.split('/')[-1]
            real_class = self.Categories[self.true_label[index]]
            if real_class == 'NL':
                real_class = 'Unlabeled'
            data.append([p_name, str(i), real_class, 1])
            index += 1
        self.cluster_overview = pd.DataFrame(data=data, columns=field_names)
        self.groupby_ground_truth = self.cluster_overview.groupby(
            by=['Ground truth', 'Cluster']).sum()
        self.groupby_Stempel = self.cluster_overview.groupby(
            by=['Cluster', 'Ground truth']).sum()

    def plot_overview(self, cluster_label, ylabel_size=2):
        """
            plots a bar chart of cluster with label "cluster_label"
            ylabel_size: change size of the y axis
        """
        title = "Cluster " + str(cluster_label)
        sns.set_style(
            "darkgrid", {"xtick.major.size": 8, "ytick.major.size": 8})
        sns.countplot(x='Ground truth', hue='Ground truth', dodge=False,
                      data=self.cluster_overview.loc[self.cluster_overview['Cluster'] == cluster_label])
        plt.ylabel("Number of Pictures")
        plt.xlabel("")
        plt.gca().yaxis.set_major_locator(mticker.MultipleLocator(ylabel_size))
        plt.title(title, size=12)
        plt.gca().axes.get_xaxis().set_ticks([])

    def plot_location(self, ground_truth, ylabel_size=2):
        """
            plots a bar chart where the label ground_truht is located in the Cluster
            ylabel_size: change size of the y axis
        """
        title = "ground truth " + str(ground_truth)
        sns.set_style(
            "darkgrid", {"xtick.major.size": 8, "ytick.major.size": 8})
        sns.countplot(x='Cluster', hue='Cluster', dodge=False,
                      data=self.cluster_overview.loc[self.cluster_overview['Ground truth'] == ground_truth])
        plt.ylabel("Number of Pictures")
        plt.xlabel("")
        plt.gca().yaxis.set_major_locator(
            mticker.MultipleLocator(ylabel_size))
        plt.title(title, size=12)
        plt.gca().axes.get_xaxis().set_ticks([])

    def plot_all_overview(self, ylabel_size=2.5, pics_per_row=4):
        """
            plots a all overview plots in one
            ylabel_size: change size of the y axis
            pics_per_row: specifies how many plot per row are shown
        """
        sns.set_style(
            "darkgrid", {"xtick.major.size": 8, "ytick.major.size": 8})
        cluster_size = len(set(self.labels))
        num_cols = pics_per_row
        num_rows = cluster_size // num_cols + 1
        fig, ax = plt.subplots(ncols=num_cols, nrows=num_rows, figsize=(
            5*num_cols, 5*num_rows), constrained_layout=True)
        for x, cluster_label in enumerate(set(self.labels)):
            cluster_label = str(cluster_label)
            col = x % num_cols
            row = x // num_cols
            plot = sns.countplot(x='Ground truth', hue='Ground truth', dodge=False,
                                 data=self.cluster_overview.loc[self.cluster_overview['Cluster']
                                                                == cluster_label], ax=ax[row][col])
            title = 'Cluster ' + cluster_label
            ax[row][col].title.set_text(title)
            ax[row][col].set_xticklabels([])
            ax[row][col].yaxis.set_major_locator(
                mticker.MultipleLocator(ylabel_size))

    def plot_all_location(self, ylabel_size=2.5, pics_per_row=6):
        """
            plots a all location plots in one
            ylabel_size: change size of the y axis
            pics_per_row: specifies how many plot per row are shown
        """
        sns.set_style(
            "darkgrid", {"xtick.major.size": 8, "ytick.major.size": 8})
        num_labels = len(self.Categories)
        num_cols = pics_per_row
        num_rows = num_labels // num_cols + 1
        fig, ax = plt.subplots(ncols=num_cols, nrows=num_rows, figsize=(
            5*num_cols, 5*num_rows), constrained_layout=True)
        for x, label in enumerate(self.Categories):
            label = str(label)
            col = x % num_cols
            row = x // num_cols
            p = sns.countplot(x='Cluster', hue='Cluster', dodge=False,
                              data=self.cluster_overview.loc[self.cluster_overview['Ground truth']
                                                             == label],
                              ax=ax[row][col])
            title = 'Ground truth ' + label
            ax[row][col].title.set_text(title)
            ax[row][col].set_xticklabels([])
            ax[row][col].yaxis.set_major_locator(
                mticker.MultipleLocator(ylabel_size))

    def save_ex_features(self, save_path):
        """
            Saves the extracted features.
            This can save a lot of time !!!
        """

        np.save(save_path, self.ex_features)

    def load_ex_features(self, save_path):
        """
            Loads saved extracted features
        """
        self.ex_features = np.load(save_path)
        self.tsne = TSNE(n_iter=3000).fit_transform(
            self.ex_features)
